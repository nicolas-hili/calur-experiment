package ca.queensu.cs.umlrt.calur.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ca.queensu.cs.umlrt.calur.services.CalurGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCalurParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_FLOAT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'verbatim'", "';'", "'send'", "'message'", "'to'", "'port'", "'with'", "'parameters'", "'log'", "':'", "'inform'", "'in'", "'and'", "'at'", "'every'", "'cancel'", "'incarnate'", "'destroy'", "'import'", "'deport'", "'register'", "'deregister'", "'('", "')'", "'var'", "'call'", "'return'", "'srand'", "':='", "'this.'", "'.'", "'if'", "'then'", "'{'", "'}'", "'else'", "'()'", "'not'", "'<'", "'>'", "'<='", "'>='", "'='", "'<>'", "'sqrt'", "'abs'", "'ceil'", "'floor'", "'rand'", "'capsuleName'", "'capsulePartName'", "'index'", "'+'", "'-'", "'*'", "'/'", "'%'", "'null'", "'true'", "'false'", "'seconds'", "'milliseconds'", "'microseconds'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int RULE_FLOAT=7;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalCalurParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCalurParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCalurParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCalur.g"; }



     	private CalurGrammarAccess grammarAccess;

        public InternalCalurParser(TokenStream input, CalurGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Action";
       	}

       	@Override
       	protected CalurGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAction"
    // InternalCalur.g:65:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalCalur.g:65:47: (iv_ruleAction= ruleAction EOF )
            // InternalCalur.g:66:2: iv_ruleAction= ruleAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalCalur.g:72:1: ruleAction returns [EObject current=null] : ( (lv_statements_0_0= ruleStatement ) )* ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        EObject lv_statements_0_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:78:2: ( ( (lv_statements_0_0= ruleStatement ) )* )
            // InternalCalur.g:79:2: ( (lv_statements_0_0= ruleStatement ) )*
            {
            // InternalCalur.g:79:2: ( (lv_statements_0_0= ruleStatement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||LA1_0==12||LA1_0==14||LA1_0==20||LA1_0==22||(LA1_0>=27 && LA1_0<=33)||(LA1_0>=36 && LA1_0<=39)||LA1_0==41||LA1_0==43) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCalur.g:80:3: (lv_statements_0_0= ruleStatement )
            	    {
            	    // InternalCalur.g:80:3: (lv_statements_0_0= ruleStatement )
            	    // InternalCalur.g:81:4: lv_statements_0_0= ruleStatement
            	    {
            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getActionAccess().getStatementsStatementParserRuleCall_0());
            	      			
            	    }
            	    pushFollow(FOLLOW_3);
            	    lv_statements_0_0=ruleStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				if (current==null) {
            	      					current = createModelElementForParent(grammarAccess.getActionRule());
            	      				}
            	      				add(
            	      					current,
            	      					"statements",
            	      					lv_statements_0_0,
            	      					"ca.queensu.cs.umlrt.calur.Calur.Statement");
            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleStatement"
    // InternalCalur.g:101:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // InternalCalur.g:101:50: (iv_ruleStatement= ruleStatement EOF )
            // InternalCalur.g:102:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalCalur.g:108:1: ruleStatement returns [EObject current=null] : (this_SendStatement_0= ruleSendStatement | this_LogStatement_1= ruleLogStatement | this_IncarnateStatement_2= ruleIncarnateStatement | this_DestroyStatement_3= ruleDestroyStatement | this_ImportStatement_4= ruleImportStatement | this_TimerStatement_5= ruleTimerStatement | this_CancelTimerStatement_6= ruleCancelTimerStatement | this_DeportStatement_7= ruleDeportStatement | this_RegisterStatement_8= ruleRegisterStatement | this_DeregisterStatement_9= ruleDeregisterStatement | this_VariableInitializationStatement_10= ruleVariableInitializationStatement | this_VariableAffectationStatement_11= ruleVariableAffectationStatement | this_CallStatement_12= ruleCallStatement | this_ObjectCallOperation_13= ruleObjectCallOperation | this_IfStatement_14= ruleIfStatement | this_VerbatimStatement_15= ruleVerbatimStatement | this_ReturnStatement_16= ruleReturnStatement | this_SrandStatement_17= ruleSrandStatement ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_SendStatement_0 = null;

        EObject this_LogStatement_1 = null;

        EObject this_IncarnateStatement_2 = null;

        EObject this_DestroyStatement_3 = null;

        EObject this_ImportStatement_4 = null;

        EObject this_TimerStatement_5 = null;

        EObject this_CancelTimerStatement_6 = null;

        EObject this_DeportStatement_7 = null;

        EObject this_RegisterStatement_8 = null;

        EObject this_DeregisterStatement_9 = null;

        EObject this_VariableInitializationStatement_10 = null;

        EObject this_VariableAffectationStatement_11 = null;

        EObject this_CallStatement_12 = null;

        EObject this_ObjectCallOperation_13 = null;

        EObject this_IfStatement_14 = null;

        EObject this_VerbatimStatement_15 = null;

        EObject this_ReturnStatement_16 = null;

        EObject this_SrandStatement_17 = null;



        	enterRule();

        try {
            // InternalCalur.g:114:2: ( (this_SendStatement_0= ruleSendStatement | this_LogStatement_1= ruleLogStatement | this_IncarnateStatement_2= ruleIncarnateStatement | this_DestroyStatement_3= ruleDestroyStatement | this_ImportStatement_4= ruleImportStatement | this_TimerStatement_5= ruleTimerStatement | this_CancelTimerStatement_6= ruleCancelTimerStatement | this_DeportStatement_7= ruleDeportStatement | this_RegisterStatement_8= ruleRegisterStatement | this_DeregisterStatement_9= ruleDeregisterStatement | this_VariableInitializationStatement_10= ruleVariableInitializationStatement | this_VariableAffectationStatement_11= ruleVariableAffectationStatement | this_CallStatement_12= ruleCallStatement | this_ObjectCallOperation_13= ruleObjectCallOperation | this_IfStatement_14= ruleIfStatement | this_VerbatimStatement_15= ruleVerbatimStatement | this_ReturnStatement_16= ruleReturnStatement | this_SrandStatement_17= ruleSrandStatement ) )
            // InternalCalur.g:115:2: (this_SendStatement_0= ruleSendStatement | this_LogStatement_1= ruleLogStatement | this_IncarnateStatement_2= ruleIncarnateStatement | this_DestroyStatement_3= ruleDestroyStatement | this_ImportStatement_4= ruleImportStatement | this_TimerStatement_5= ruleTimerStatement | this_CancelTimerStatement_6= ruleCancelTimerStatement | this_DeportStatement_7= ruleDeportStatement | this_RegisterStatement_8= ruleRegisterStatement | this_DeregisterStatement_9= ruleDeregisterStatement | this_VariableInitializationStatement_10= ruleVariableInitializationStatement | this_VariableAffectationStatement_11= ruleVariableAffectationStatement | this_CallStatement_12= ruleCallStatement | this_ObjectCallOperation_13= ruleObjectCallOperation | this_IfStatement_14= ruleIfStatement | this_VerbatimStatement_15= ruleVerbatimStatement | this_ReturnStatement_16= ruleReturnStatement | this_SrandStatement_17= ruleSrandStatement )
            {
            // InternalCalur.g:115:2: (this_SendStatement_0= ruleSendStatement | this_LogStatement_1= ruleLogStatement | this_IncarnateStatement_2= ruleIncarnateStatement | this_DestroyStatement_3= ruleDestroyStatement | this_ImportStatement_4= ruleImportStatement | this_TimerStatement_5= ruleTimerStatement | this_CancelTimerStatement_6= ruleCancelTimerStatement | this_DeportStatement_7= ruleDeportStatement | this_RegisterStatement_8= ruleRegisterStatement | this_DeregisterStatement_9= ruleDeregisterStatement | this_VariableInitializationStatement_10= ruleVariableInitializationStatement | this_VariableAffectationStatement_11= ruleVariableAffectationStatement | this_CallStatement_12= ruleCallStatement | this_ObjectCallOperation_13= ruleObjectCallOperation | this_IfStatement_14= ruleIfStatement | this_VerbatimStatement_15= ruleVerbatimStatement | this_ReturnStatement_16= ruleReturnStatement | this_SrandStatement_17= ruleSrandStatement )
            int alt2=18;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // InternalCalur.g:116:3: this_SendStatement_0= ruleSendStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getSendStatementParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SendStatement_0=ruleSendStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SendStatement_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:125:3: this_LogStatement_1= ruleLogStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getLogStatementParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LogStatement_1=ruleLogStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LogStatement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:134:3: this_IncarnateStatement_2= ruleIncarnateStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getIncarnateStatementParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_IncarnateStatement_2=ruleIncarnateStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IncarnateStatement_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:143:3: this_DestroyStatement_3= ruleDestroyStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getDestroyStatementParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DestroyStatement_3=ruleDestroyStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DestroyStatement_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalCalur.g:152:3: this_ImportStatement_4= ruleImportStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getImportStatementParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ImportStatement_4=ruleImportStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ImportStatement_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalCalur.g:161:3: this_TimerStatement_5= ruleTimerStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getTimerStatementParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_TimerStatement_5=ruleTimerStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_TimerStatement_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalCalur.g:170:3: this_CancelTimerStatement_6= ruleCancelTimerStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getCancelTimerStatementParserRuleCall_6());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CancelTimerStatement_6=ruleCancelTimerStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CancelTimerStatement_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalCalur.g:179:3: this_DeportStatement_7= ruleDeportStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getDeportStatementParserRuleCall_7());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DeportStatement_7=ruleDeportStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DeportStatement_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalCalur.g:188:3: this_RegisterStatement_8= ruleRegisterStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getRegisterStatementParserRuleCall_8());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_RegisterStatement_8=ruleRegisterStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RegisterStatement_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 10 :
                    // InternalCalur.g:197:3: this_DeregisterStatement_9= ruleDeregisterStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getDeregisterStatementParserRuleCall_9());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DeregisterStatement_9=ruleDeregisterStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DeregisterStatement_9;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 11 :
                    // InternalCalur.g:206:3: this_VariableInitializationStatement_10= ruleVariableInitializationStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getVariableInitializationStatementParserRuleCall_10());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_VariableInitializationStatement_10=ruleVariableInitializationStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_VariableInitializationStatement_10;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 12 :
                    // InternalCalur.g:215:3: this_VariableAffectationStatement_11= ruleVariableAffectationStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getVariableAffectationStatementParserRuleCall_11());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_VariableAffectationStatement_11=ruleVariableAffectationStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_VariableAffectationStatement_11;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 13 :
                    // InternalCalur.g:224:3: this_CallStatement_12= ruleCallStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getCallStatementParserRuleCall_12());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallStatement_12=ruleCallStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallStatement_12;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 14 :
                    // InternalCalur.g:233:3: this_ObjectCallOperation_13= ruleObjectCallOperation
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getObjectCallOperationParserRuleCall_13());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ObjectCallOperation_13=ruleObjectCallOperation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ObjectCallOperation_13;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 15 :
                    // InternalCalur.g:242:3: this_IfStatement_14= ruleIfStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getIfStatementParserRuleCall_14());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_IfStatement_14=ruleIfStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IfStatement_14;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 16 :
                    // InternalCalur.g:251:3: this_VerbatimStatement_15= ruleVerbatimStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getVerbatimStatementParserRuleCall_15());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_VerbatimStatement_15=ruleVerbatimStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_VerbatimStatement_15;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 17 :
                    // InternalCalur.g:260:3: this_ReturnStatement_16= ruleReturnStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getReturnStatementParserRuleCall_16());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ReturnStatement_16=ruleReturnStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ReturnStatement_16;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 18 :
                    // InternalCalur.g:269:3: this_SrandStatement_17= ruleSrandStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getSrandStatementParserRuleCall_17());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SrandStatement_17=ruleSrandStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SrandStatement_17;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleVerbatimStatement"
    // InternalCalur.g:281:1: entryRuleVerbatimStatement returns [EObject current=null] : iv_ruleVerbatimStatement= ruleVerbatimStatement EOF ;
    public final EObject entryRuleVerbatimStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVerbatimStatement = null;


        try {
            // InternalCalur.g:281:58: (iv_ruleVerbatimStatement= ruleVerbatimStatement EOF )
            // InternalCalur.g:282:2: iv_ruleVerbatimStatement= ruleVerbatimStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVerbatimStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVerbatimStatement=ruleVerbatimStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVerbatimStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVerbatimStatement"


    // $ANTLR start "ruleVerbatimStatement"
    // InternalCalur.g:288:1: ruleVerbatimStatement returns [EObject current=null] : (otherlv_0= 'verbatim' ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleVerbatimStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalCalur.g:294:2: ( (otherlv_0= 'verbatim' ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalCalur.g:295:2: (otherlv_0= 'verbatim' ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:295:2: (otherlv_0= 'verbatim' ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalCalur.g:296:3: otherlv_0= 'verbatim' ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getVerbatimStatementAccess().getVerbatimKeyword_0());
              		
            }
            // InternalCalur.g:300:3: ( (lv_value_1_0= RULE_STRING ) )
            // InternalCalur.g:301:4: (lv_value_1_0= RULE_STRING )
            {
            // InternalCalur.g:301:4: (lv_value_1_0= RULE_STRING )
            // InternalCalur.g:302:5: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getVerbatimStatementAccess().getValueSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVerbatimStatementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getVerbatimStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVerbatimStatement"


    // $ANTLR start "entryRuleSendStatement"
    // InternalCalur.g:326:1: entryRuleSendStatement returns [EObject current=null] : iv_ruleSendStatement= ruleSendStatement EOF ;
    public final EObject entryRuleSendStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSendStatement = null;


        try {
            // InternalCalur.g:326:54: (iv_ruleSendStatement= ruleSendStatement EOF )
            // InternalCalur.g:327:2: iv_ruleSendStatement= ruleSendStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSendStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSendStatement=ruleSendStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSendStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSendStatement"


    // $ANTLR start "ruleSendStatement"
    // InternalCalur.g:333:1: ruleSendStatement returns [EObject current=null] : (otherlv_0= 'send' (otherlv_1= 'message' )? ( (otherlv_2= RULE_ID ) ) otherlv_3= 'to' (otherlv_4= 'port' )? ( (lv_portRef_5_0= rulePortRef ) ) (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )? otherlv_9= ';' ) ;
    public final EObject ruleSendStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_portRef_5_0 = null;

        EObject lv_arguments_8_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:339:2: ( (otherlv_0= 'send' (otherlv_1= 'message' )? ( (otherlv_2= RULE_ID ) ) otherlv_3= 'to' (otherlv_4= 'port' )? ( (lv_portRef_5_0= rulePortRef ) ) (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )? otherlv_9= ';' ) )
            // InternalCalur.g:340:2: (otherlv_0= 'send' (otherlv_1= 'message' )? ( (otherlv_2= RULE_ID ) ) otherlv_3= 'to' (otherlv_4= 'port' )? ( (lv_portRef_5_0= rulePortRef ) ) (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )? otherlv_9= ';' )
            {
            // InternalCalur.g:340:2: (otherlv_0= 'send' (otherlv_1= 'message' )? ( (otherlv_2= RULE_ID ) ) otherlv_3= 'to' (otherlv_4= 'port' )? ( (lv_portRef_5_0= rulePortRef ) ) (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )? otherlv_9= ';' )
            // InternalCalur.g:341:3: otherlv_0= 'send' (otherlv_1= 'message' )? ( (otherlv_2= RULE_ID ) ) otherlv_3= 'to' (otherlv_4= 'port' )? ( (lv_portRef_5_0= rulePortRef ) ) (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )? otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSendStatementAccess().getSendKeyword_0());
              		
            }
            // InternalCalur.g:345:3: (otherlv_1= 'message' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalCalur.g:346:4: otherlv_1= 'message'
                    {
                    otherlv_1=(Token)match(input,15,FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getSendStatementAccess().getMessageKeyword_1());
                      			
                    }

                    }
                    break;

            }

            // InternalCalur.g:351:3: ( (otherlv_2= RULE_ID ) )
            // InternalCalur.g:352:4: (otherlv_2= RULE_ID )
            {
            // InternalCalur.g:352:4: (otherlv_2= RULE_ID )
            // InternalCalur.g:353:5: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSendStatementRule());
              					}
              				
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_2, grammarAccess.getSendStatementAccess().getOperationOperationCrossReference_2_0());
              				
            }

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getSendStatementAccess().getToKeyword_3());
              		
            }
            // InternalCalur.g:368:3: (otherlv_4= 'port' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalCalur.g:369:4: otherlv_4= 'port'
                    {
                    otherlv_4=(Token)match(input,17,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getSendStatementAccess().getPortKeyword_4());
                      			
                    }

                    }
                    break;

            }

            // InternalCalur.g:374:3: ( (lv_portRef_5_0= rulePortRef ) )
            // InternalCalur.g:375:4: (lv_portRef_5_0= rulePortRef )
            {
            // InternalCalur.g:375:4: (lv_portRef_5_0= rulePortRef )
            // InternalCalur.g:376:5: lv_portRef_5_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSendStatementAccess().getPortRefPortRefParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_portRef_5_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSendStatementRule());
              					}
              					set(
              						current,
              						"portRef",
              						lv_portRef_5_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:393:3: (otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalCalur.g:394:4: otherlv_6= 'with' (otherlv_7= 'parameters' )? ( (lv_arguments_8_0= ruleArguments ) )
                    {
                    otherlv_6=(Token)match(input,18,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getSendStatementAccess().getWithKeyword_6_0());
                      			
                    }
                    // InternalCalur.g:398:4: (otherlv_7= 'parameters' )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==19) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalCalur.g:399:5: otherlv_7= 'parameters'
                            {
                            otherlv_7=(Token)match(input,19,FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_7, grammarAccess.getSendStatementAccess().getParametersKeyword_6_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalCalur.g:404:4: ( (lv_arguments_8_0= ruleArguments ) )
                    // InternalCalur.g:405:5: (lv_arguments_8_0= ruleArguments )
                    {
                    // InternalCalur.g:405:5: (lv_arguments_8_0= ruleArguments )
                    // InternalCalur.g:406:6: lv_arguments_8_0= ruleArguments
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getSendStatementAccess().getArgumentsArgumentsParserRuleCall_6_2_0());
                      					
                    }
                    pushFollow(FOLLOW_5);
                    lv_arguments_8_0=ruleArguments();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getSendStatementRule());
                      						}
                      						set(
                      							current,
                      							"arguments",
                      							lv_arguments_8_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.Arguments");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getSendStatementAccess().getSemicolonKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSendStatement"


    // $ANTLR start "entryRuleLogStatement"
    // InternalCalur.g:432:1: entryRuleLogStatement returns [EObject current=null] : iv_ruleLogStatement= ruleLogStatement EOF ;
    public final EObject entryRuleLogStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogStatement = null;


        try {
            // InternalCalur.g:432:53: (iv_ruleLogStatement= ruleLogStatement EOF )
            // InternalCalur.g:433:2: iv_ruleLogStatement= ruleLogStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLogStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLogStatement=ruleLogStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLogStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogStatement"


    // $ANTLR start "ruleLogStatement"
    // InternalCalur.g:439:1: ruleLogStatement returns [EObject current=null] : (otherlv_0= 'log' ( (lv_value_1_0= RULE_STRING ) ) (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )? otherlv_5= ';' ) ;
    public final EObject ruleLogStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:445:2: ( (otherlv_0= 'log' ( (lv_value_1_0= RULE_STRING ) ) (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )? otherlv_5= ';' ) )
            // InternalCalur.g:446:2: (otherlv_0= 'log' ( (lv_value_1_0= RULE_STRING ) ) (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )? otherlv_5= ';' )
            {
            // InternalCalur.g:446:2: (otherlv_0= 'log' ( (lv_value_1_0= RULE_STRING ) ) (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )? otherlv_5= ';' )
            // InternalCalur.g:447:3: otherlv_0= 'log' ( (lv_value_1_0= RULE_STRING ) ) (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )? otherlv_5= ';'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getLogStatementAccess().getLogKeyword_0());
              		
            }
            // InternalCalur.g:451:3: ( (lv_value_1_0= RULE_STRING ) )
            // InternalCalur.g:452:4: (lv_value_1_0= RULE_STRING )
            {
            // InternalCalur.g:452:4: (lv_value_1_0= RULE_STRING )
            // InternalCalur.g:453:5: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getLogStatementAccess().getValueSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLogStatementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            // InternalCalur.g:469:3: (otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalCalur.g:470:4: otherlv_2= 'with' (otherlv_3= 'parameters' )? ( (lv_arguments_4_0= ruleArguments ) )
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getLogStatementAccess().getWithKeyword_2_0());
                      			
                    }
                    // InternalCalur.g:474:4: (otherlv_3= 'parameters' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==19) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalCalur.g:475:5: otherlv_3= 'parameters'
                            {
                            otherlv_3=(Token)match(input,19,FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_3, grammarAccess.getLogStatementAccess().getParametersKeyword_2_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalCalur.g:480:4: ( (lv_arguments_4_0= ruleArguments ) )
                    // InternalCalur.g:481:5: (lv_arguments_4_0= ruleArguments )
                    {
                    // InternalCalur.g:481:5: (lv_arguments_4_0= ruleArguments )
                    // InternalCalur.g:482:6: lv_arguments_4_0= ruleArguments
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getLogStatementAccess().getArgumentsArgumentsParserRuleCall_2_2_0());
                      					
                    }
                    pushFollow(FOLLOW_5);
                    lv_arguments_4_0=ruleArguments();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getLogStatementRule());
                      						}
                      						set(
                      							current,
                      							"arguments",
                      							lv_arguments_4_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.Arguments");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getLogStatementAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogStatement"


    // $ANTLR start "entryRuleTimerStatement"
    // InternalCalur.g:508:1: entryRuleTimerStatement returns [EObject current=null] : iv_ruleTimerStatement= ruleTimerStatement EOF ;
    public final EObject entryRuleTimerStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimerStatement = null;


        try {
            // InternalCalur.g:508:55: (iv_ruleTimerStatement= ruleTimerStatement EOF )
            // InternalCalur.g:509:2: iv_ruleTimerStatement= ruleTimerStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimerStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimerStatement=ruleTimerStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimerStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimerStatement"


    // $ANTLR start "ruleTimerStatement"
    // InternalCalur.g:515:1: ruleTimerStatement returns [EObject current=null] : (this_InformInTimerStatement_0= ruleInformInTimerStatement | this_InformEveryTimerStatement_1= ruleInformEveryTimerStatement ) ;
    public final EObject ruleTimerStatement() throws RecognitionException {
        EObject current = null;

        EObject this_InformInTimerStatement_0 = null;

        EObject this_InformEveryTimerStatement_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:521:2: ( (this_InformInTimerStatement_0= ruleInformInTimerStatement | this_InformEveryTimerStatement_1= ruleInformEveryTimerStatement ) )
            // InternalCalur.g:522:2: (this_InformInTimerStatement_0= ruleInformInTimerStatement | this_InformEveryTimerStatement_1= ruleInformEveryTimerStatement )
            {
            // InternalCalur.g:522:2: (this_InformInTimerStatement_0= ruleInformInTimerStatement | this_InformEveryTimerStatement_1= ruleInformEveryTimerStatement )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==21) ) {
                    int LA9_3 = input.LA(3);

                    if ( (LA9_3==22) ) {
                        int LA9_2 = input.LA(4);

                        if ( (LA9_2==23) ) {
                            alt9=1;
                        }
                        else if ( (LA9_2==26) ) {
                            alt9=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 9, 2, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 9, 3, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA9_0==22) ) {
                int LA9_2 = input.LA(2);

                if ( (LA9_2==23) ) {
                    alt9=1;
                }
                else if ( (LA9_2==26) ) {
                    alt9=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalCalur.g:523:3: this_InformInTimerStatement_0= ruleInformInTimerStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimerStatementAccess().getInformInTimerStatementParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InformInTimerStatement_0=ruleInformInTimerStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InformInTimerStatement_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:532:3: this_InformEveryTimerStatement_1= ruleInformEveryTimerStatement
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTimerStatementAccess().getInformEveryTimerStatementParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InformEveryTimerStatement_1=ruleInformEveryTimerStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InformEveryTimerStatement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimerStatement"


    // $ANTLR start "entryRuleInformInTimerStatement"
    // InternalCalur.g:544:1: entryRuleInformInTimerStatement returns [EObject current=null] : iv_ruleInformInTimerStatement= ruleInformInTimerStatement EOF ;
    public final EObject entryRuleInformInTimerStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInformInTimerStatement = null;


        try {
            // InternalCalur.g:544:63: (iv_ruleInformInTimerStatement= ruleInformInTimerStatement EOF )
            // InternalCalur.g:545:2: iv_ruleInformInTimerStatement= ruleInformInTimerStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInformInTimerStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInformInTimerStatement=ruleInformInTimerStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInformInTimerStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInformInTimerStatement"


    // $ANTLR start "ruleInformInTimerStatement"
    // InternalCalur.g:551:1: ruleInformInTimerStatement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'in' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' ) ;
    public final EObject ruleInformInTimerStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_timeSpec_4_0 = null;

        EObject lv_timeSpec_6_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:557:2: ( ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'in' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' ) )
            // InternalCalur.g:558:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'in' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' )
            {
            // InternalCalur.g:558:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'in' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' )
            // InternalCalur.g:559:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'in' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';'
            {
            // InternalCalur.g:559:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalCalur.g:560:4: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':'
                    {
                    // InternalCalur.g:560:4: ( (otherlv_0= RULE_ID ) )
                    // InternalCalur.g:561:5: (otherlv_0= RULE_ID )
                    {
                    // InternalCalur.g:561:5: (otherlv_0= RULE_ID )
                    // InternalCalur.g:562:6: otherlv_0= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInformInTimerStatementRule());
                      						}
                      					
                    }
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_0, grammarAccess.getInformInTimerStatementAccess().getTimerIdPropertyCrossReference_0_0_0());
                      					
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,21,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getInformInTimerStatementAccess().getColonKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,22,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInformInTimerStatementAccess().getInformKeyword_1());
              		
            }
            otherlv_3=(Token)match(input,23,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getInformInTimerStatementAccess().getInKeyword_2());
              		
            }
            // InternalCalur.g:586:3: ( (lv_timeSpec_4_0= ruleTimeSpec ) )
            // InternalCalur.g:587:4: (lv_timeSpec_4_0= ruleTimeSpec )
            {
            // InternalCalur.g:587:4: (lv_timeSpec_4_0= ruleTimeSpec )
            // InternalCalur.g:588:5: lv_timeSpec_4_0= ruleTimeSpec
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInformInTimerStatementAccess().getTimeSpecTimeSpecParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_timeSpec_4_0=ruleTimeSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInformInTimerStatementRule());
              					}
              					add(
              						current,
              						"timeSpec",
              						lv_timeSpec_4_0,
              						"ca.queensu.cs.umlrt.calur.Calur.TimeSpec");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:605:3: (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==24) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalCalur.g:606:4: otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) )
            	    {
            	    otherlv_5=(Token)match(input,24,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_5, grammarAccess.getInformInTimerStatementAccess().getAndKeyword_4_0());
            	      			
            	    }
            	    // InternalCalur.g:610:4: ( (lv_timeSpec_6_0= ruleTimeSpec ) )
            	    // InternalCalur.g:611:5: (lv_timeSpec_6_0= ruleTimeSpec )
            	    {
            	    // InternalCalur.g:611:5: (lv_timeSpec_6_0= ruleTimeSpec )
            	    // InternalCalur.g:612:6: lv_timeSpec_6_0= ruleTimeSpec
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getInformInTimerStatementAccess().getTimeSpecTimeSpecParserRuleCall_4_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_timeSpec_6_0=ruleTimeSpec();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getInformInTimerStatementRule());
            	      						}
            	      						add(
            	      							current,
            	      							"timeSpec",
            	      							lv_timeSpec_6_0,
            	      							"ca.queensu.cs.umlrt.calur.Calur.TimeSpec");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalCalur.g:630:3: (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==25) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalCalur.g:631:4: otherlv_7= 'at' ( (otherlv_8= RULE_ID ) )
                    {
                    otherlv_7=(Token)match(input,25,FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getInformInTimerStatementAccess().getAtKeyword_5_0());
                      			
                    }
                    // InternalCalur.g:635:4: ( (otherlv_8= RULE_ID ) )
                    // InternalCalur.g:636:5: (otherlv_8= RULE_ID )
                    {
                    // InternalCalur.g:636:5: (otherlv_8= RULE_ID )
                    // InternalCalur.g:637:6: otherlv_8= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInformInTimerStatementRule());
                      						}
                      					
                    }
                    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_8, grammarAccess.getInformInTimerStatementAccess().getPortPortCrossReference_5_1_0());
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getInformInTimerStatementAccess().getSemicolonKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInformInTimerStatement"


    // $ANTLR start "entryRuleInformEveryTimerStatement"
    // InternalCalur.g:657:1: entryRuleInformEveryTimerStatement returns [EObject current=null] : iv_ruleInformEveryTimerStatement= ruleInformEveryTimerStatement EOF ;
    public final EObject entryRuleInformEveryTimerStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInformEveryTimerStatement = null;


        try {
            // InternalCalur.g:657:66: (iv_ruleInformEveryTimerStatement= ruleInformEveryTimerStatement EOF )
            // InternalCalur.g:658:2: iv_ruleInformEveryTimerStatement= ruleInformEveryTimerStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInformEveryTimerStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInformEveryTimerStatement=ruleInformEveryTimerStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInformEveryTimerStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInformEveryTimerStatement"


    // $ANTLR start "ruleInformEveryTimerStatement"
    // InternalCalur.g:664:1: ruleInformEveryTimerStatement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'every' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' ) ;
    public final EObject ruleInformEveryTimerStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_timeSpec_4_0 = null;

        EObject lv_timeSpec_6_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:670:2: ( ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'every' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' ) )
            // InternalCalur.g:671:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'every' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' )
            {
            // InternalCalur.g:671:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'every' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';' )
            // InternalCalur.g:672:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'inform' otherlv_3= 'every' ( (lv_timeSpec_4_0= ruleTimeSpec ) ) (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )* (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )? otherlv_9= ';'
            {
            // InternalCalur.g:672:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalCalur.g:673:4: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':'
                    {
                    // InternalCalur.g:673:4: ( (otherlv_0= RULE_ID ) )
                    // InternalCalur.g:674:5: (otherlv_0= RULE_ID )
                    {
                    // InternalCalur.g:674:5: (otherlv_0= RULE_ID )
                    // InternalCalur.g:675:6: otherlv_0= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInformEveryTimerStatementRule());
                      						}
                      					
                    }
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_0, grammarAccess.getInformEveryTimerStatementAccess().getTimerIdPropertyCrossReference_0_0_0());
                      					
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,21,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getInformEveryTimerStatementAccess().getColonKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,22,FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInformEveryTimerStatementAccess().getInformKeyword_1());
              		
            }
            otherlv_3=(Token)match(input,26,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getInformEveryTimerStatementAccess().getEveryKeyword_2());
              		
            }
            // InternalCalur.g:699:3: ( (lv_timeSpec_4_0= ruleTimeSpec ) )
            // InternalCalur.g:700:4: (lv_timeSpec_4_0= ruleTimeSpec )
            {
            // InternalCalur.g:700:4: (lv_timeSpec_4_0= ruleTimeSpec )
            // InternalCalur.g:701:5: lv_timeSpec_4_0= ruleTimeSpec
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInformEveryTimerStatementAccess().getTimeSpecTimeSpecParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_timeSpec_4_0=ruleTimeSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInformEveryTimerStatementRule());
              					}
              					add(
              						current,
              						"timeSpec",
              						lv_timeSpec_4_0,
              						"ca.queensu.cs.umlrt.calur.Calur.TimeSpec");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:718:3: (otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==24) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalCalur.g:719:4: otherlv_5= 'and' ( (lv_timeSpec_6_0= ruleTimeSpec ) )
            	    {
            	    otherlv_5=(Token)match(input,24,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_5, grammarAccess.getInformEveryTimerStatementAccess().getAndKeyword_4_0());
            	      			
            	    }
            	    // InternalCalur.g:723:4: ( (lv_timeSpec_6_0= ruleTimeSpec ) )
            	    // InternalCalur.g:724:5: (lv_timeSpec_6_0= ruleTimeSpec )
            	    {
            	    // InternalCalur.g:724:5: (lv_timeSpec_6_0= ruleTimeSpec )
            	    // InternalCalur.g:725:6: lv_timeSpec_6_0= ruleTimeSpec
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getInformEveryTimerStatementAccess().getTimeSpecTimeSpecParserRuleCall_4_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_timeSpec_6_0=ruleTimeSpec();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getInformEveryTimerStatementRule());
            	      						}
            	      						add(
            	      							current,
            	      							"timeSpec",
            	      							lv_timeSpec_6_0,
            	      							"ca.queensu.cs.umlrt.calur.Calur.TimeSpec");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            // InternalCalur.g:743:3: (otherlv_7= 'at' ( (otherlv_8= RULE_ID ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==25) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalCalur.g:744:4: otherlv_7= 'at' ( (otherlv_8= RULE_ID ) )
                    {
                    otherlv_7=(Token)match(input,25,FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getInformEveryTimerStatementAccess().getAtKeyword_5_0());
                      			
                    }
                    // InternalCalur.g:748:4: ( (otherlv_8= RULE_ID ) )
                    // InternalCalur.g:749:5: (otherlv_8= RULE_ID )
                    {
                    // InternalCalur.g:749:5: (otherlv_8= RULE_ID )
                    // InternalCalur.g:750:6: otherlv_8= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInformEveryTimerStatementRule());
                      						}
                      					
                    }
                    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_8, grammarAccess.getInformEveryTimerStatementAccess().getPortPortCrossReference_5_1_0());
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getInformEveryTimerStatementAccess().getSemicolonKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInformEveryTimerStatement"


    // $ANTLR start "entryRuleCancelTimerStatement"
    // InternalCalur.g:770:1: entryRuleCancelTimerStatement returns [EObject current=null] : iv_ruleCancelTimerStatement= ruleCancelTimerStatement EOF ;
    public final EObject entryRuleCancelTimerStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCancelTimerStatement = null;


        try {
            // InternalCalur.g:770:61: (iv_ruleCancelTimerStatement= ruleCancelTimerStatement EOF )
            // InternalCalur.g:771:2: iv_ruleCancelTimerStatement= ruleCancelTimerStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCancelTimerStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCancelTimerStatement=ruleCancelTimerStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCancelTimerStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCancelTimerStatement"


    // $ANTLR start "ruleCancelTimerStatement"
    // InternalCalur.g:777:1: ruleCancelTimerStatement returns [EObject current=null] : (otherlv_0= 'cancel' ( (otherlv_1= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleCancelTimerStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalCalur.g:783:2: ( (otherlv_0= 'cancel' ( (otherlv_1= RULE_ID ) ) otherlv_2= ';' ) )
            // InternalCalur.g:784:2: (otherlv_0= 'cancel' ( (otherlv_1= RULE_ID ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:784:2: (otherlv_0= 'cancel' ( (otherlv_1= RULE_ID ) ) otherlv_2= ';' )
            // InternalCalur.g:785:3: otherlv_0= 'cancel' ( (otherlv_1= RULE_ID ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCancelTimerStatementAccess().getCancelKeyword_0());
              		
            }
            // InternalCalur.g:789:3: ( (otherlv_1= RULE_ID ) )
            // InternalCalur.g:790:4: (otherlv_1= RULE_ID )
            {
            // InternalCalur.g:790:4: (otherlv_1= RULE_ID )
            // InternalCalur.g:791:5: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCancelTimerStatementRule());
              					}
              				
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_1, grammarAccess.getCancelTimerStatementAccess().getTimerIdPropertyCrossReference_1_0());
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getCancelTimerStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCancelTimerStatement"


    // $ANTLR start "entryRuleTimeSpec"
    // InternalCalur.g:810:1: entryRuleTimeSpec returns [EObject current=null] : iv_ruleTimeSpec= ruleTimeSpec EOF ;
    public final EObject entryRuleTimeSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeSpec = null;


        try {
            // InternalCalur.g:810:49: (iv_ruleTimeSpec= ruleTimeSpec EOF )
            // InternalCalur.g:811:2: iv_ruleTimeSpec= ruleTimeSpec EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeSpecRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeSpec=ruleTimeSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeSpec; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeSpec"


    // $ANTLR start "ruleTimeSpec"
    // InternalCalur.g:817:1: ruleTimeSpec returns [EObject current=null] : ( ( (lv_val_0_0= ruleBinaryExpr ) ) ( (lv_unit_1_0= ruleUnit ) ) ) ;
    public final EObject ruleTimeSpec() throws RecognitionException {
        EObject current = null;

        EObject lv_val_0_0 = null;

        Enumerator lv_unit_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:823:2: ( ( ( (lv_val_0_0= ruleBinaryExpr ) ) ( (lv_unit_1_0= ruleUnit ) ) ) )
            // InternalCalur.g:824:2: ( ( (lv_val_0_0= ruleBinaryExpr ) ) ( (lv_unit_1_0= ruleUnit ) ) )
            {
            // InternalCalur.g:824:2: ( ( (lv_val_0_0= ruleBinaryExpr ) ) ( (lv_unit_1_0= ruleUnit ) ) )
            // InternalCalur.g:825:3: ( (lv_val_0_0= ruleBinaryExpr ) ) ( (lv_unit_1_0= ruleUnit ) )
            {
            // InternalCalur.g:825:3: ( (lv_val_0_0= ruleBinaryExpr ) )
            // InternalCalur.g:826:4: (lv_val_0_0= ruleBinaryExpr )
            {
            // InternalCalur.g:826:4: (lv_val_0_0= ruleBinaryExpr )
            // InternalCalur.g:827:5: lv_val_0_0= ruleBinaryExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTimeSpecAccess().getValBinaryExprParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_17);
            lv_val_0_0=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTimeSpecRule());
              					}
              					set(
              						current,
              						"val",
              						lv_val_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:844:3: ( (lv_unit_1_0= ruleUnit ) )
            // InternalCalur.g:845:4: (lv_unit_1_0= ruleUnit )
            {
            // InternalCalur.g:845:4: (lv_unit_1_0= ruleUnit )
            // InternalCalur.g:846:5: lv_unit_1_0= ruleUnit
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTimeSpecAccess().getUnitUnitEnumRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_unit_1_0=ruleUnit();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTimeSpecRule());
              					}
              					set(
              						current,
              						"unit",
              						lv_unit_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Unit");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeSpec"


    // $ANTLR start "entryRuleIncarnateStatement"
    // InternalCalur.g:867:1: entryRuleIncarnateStatement returns [EObject current=null] : iv_ruleIncarnateStatement= ruleIncarnateStatement EOF ;
    public final EObject entryRuleIncarnateStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncarnateStatement = null;


        try {
            // InternalCalur.g:867:59: (iv_ruleIncarnateStatement= ruleIncarnateStatement EOF )
            // InternalCalur.g:868:2: iv_ruleIncarnateStatement= ruleIncarnateStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIncarnateStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIncarnateStatement=ruleIncarnateStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIncarnateStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncarnateStatement"


    // $ANTLR start "ruleIncarnateStatement"
    // InternalCalur.g:874:1: ruleIncarnateStatement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'incarnate' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' ) ;
    public final EObject ruleIncarnateStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_partRef_5_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:880:2: ( ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'incarnate' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' ) )
            // InternalCalur.g:881:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'incarnate' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' )
            {
            // InternalCalur.g:881:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'incarnate' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' )
            // InternalCalur.g:882:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'incarnate' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';'
            {
            // InternalCalur.g:882:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalCalur.g:883:4: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':'
                    {
                    // InternalCalur.g:883:4: ( (otherlv_0= RULE_ID ) )
                    // InternalCalur.g:884:5: (otherlv_0= RULE_ID )
                    {
                    // InternalCalur.g:884:5: (otherlv_0= RULE_ID )
                    // InternalCalur.g:885:6: otherlv_0= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIncarnateStatementRule());
                      						}
                      					
                    }
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_0, grammarAccess.getIncarnateStatementAccess().getCapsuleIdPropertyCrossReference_0_0_0());
                      					
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,21,FOLLOW_18); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getIncarnateStatementAccess().getColonKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,28,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getIncarnateStatementAccess().getIncarnateKeyword_1());
              		
            }
            // InternalCalur.g:905:3: ( (otherlv_3= RULE_ID ) )
            // InternalCalur.g:906:4: (otherlv_3= RULE_ID )
            {
            // InternalCalur.g:906:4: (otherlv_3= RULE_ID )
            // InternalCalur.g:907:5: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getIncarnateStatementRule());
              					}
              				
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_3, grammarAccess.getIncarnateStatementAccess().getCapsuleNameClassCrossReference_2_0());
              				
            }

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIncarnateStatementAccess().getAtKeyword_3());
              		
            }
            // InternalCalur.g:922:3: ( (lv_partRef_5_0= rulePartRef ) )
            // InternalCalur.g:923:4: (lv_partRef_5_0= rulePartRef )
            {
            // InternalCalur.g:923:4: (lv_partRef_5_0= rulePartRef )
            // InternalCalur.g:924:5: lv_partRef_5_0= rulePartRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIncarnateStatementAccess().getPartRefPartRefParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_partRef_5_0=rulePartRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIncarnateStatementRule());
              					}
              					set(
              						current,
              						"partRef",
              						lv_partRef_5_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PartRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIncarnateStatementAccess().getSemicolonKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncarnateStatement"


    // $ANTLR start "entryRuleDestroyStatement"
    // InternalCalur.g:949:1: entryRuleDestroyStatement returns [EObject current=null] : iv_ruleDestroyStatement= ruleDestroyStatement EOF ;
    public final EObject entryRuleDestroyStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDestroyStatement = null;


        try {
            // InternalCalur.g:949:57: (iv_ruleDestroyStatement= ruleDestroyStatement EOF )
            // InternalCalur.g:950:2: iv_ruleDestroyStatement= ruleDestroyStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDestroyStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDestroyStatement=ruleDestroyStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDestroyStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDestroyStatement"


    // $ANTLR start "ruleDestroyStatement"
    // InternalCalur.g:956:1: ruleDestroyStatement returns [EObject current=null] : (otherlv_0= 'destroy' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' ) ;
    public final EObject ruleDestroyStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_partRef_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:962:2: ( (otherlv_0= 'destroy' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' ) )
            // InternalCalur.g:963:2: (otherlv_0= 'destroy' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:963:2: (otherlv_0= 'destroy' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' )
            // InternalCalur.g:964:3: otherlv_0= 'destroy' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getDestroyStatementAccess().getDestroyKeyword_0());
              		
            }
            // InternalCalur.g:968:3: ( (lv_partRef_1_0= rulePartRef ) )
            // InternalCalur.g:969:4: (lv_partRef_1_0= rulePartRef )
            {
            // InternalCalur.g:969:4: (lv_partRef_1_0= rulePartRef )
            // InternalCalur.g:970:5: lv_partRef_1_0= rulePartRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDestroyStatementAccess().getPartRefPartRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_partRef_1_0=rulePartRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDestroyStatementRule());
              					}
              					set(
              						current,
              						"partRef",
              						lv_partRef_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PartRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDestroyStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDestroyStatement"


    // $ANTLR start "entryRuleImportStatement"
    // InternalCalur.g:995:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalCalur.g:995:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalCalur.g:996:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImportStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalCalur.g:1002:1: ruleImportStatement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'import' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_partRef_5_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1008:2: ( ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'import' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' ) )
            // InternalCalur.g:1009:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'import' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' )
            {
            // InternalCalur.g:1009:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'import' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';' )
            // InternalCalur.g:1010:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )? otherlv_2= 'import' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'at' ( (lv_partRef_5_0= rulePartRef ) ) otherlv_6= ';'
            {
            // InternalCalur.g:1010:3: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_ID) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalCalur.g:1011:4: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':'
                    {
                    // InternalCalur.g:1011:4: ( (otherlv_0= RULE_ID ) )
                    // InternalCalur.g:1012:5: (otherlv_0= RULE_ID )
                    {
                    // InternalCalur.g:1012:5: (otherlv_0= RULE_ID )
                    // InternalCalur.g:1013:6: otherlv_0= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getImportStatementRule());
                      						}
                      					
                    }
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getCapsuleIdPropertyCrossReference_0_0_0());
                      					
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,21,FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getImportStatementAccess().getColonKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,30,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getImportStatementAccess().getImportKeyword_1());
              		
            }
            // InternalCalur.g:1033:3: ( (otherlv_3= RULE_ID ) )
            // InternalCalur.g:1034:4: (otherlv_3= RULE_ID )
            {
            // InternalCalur.g:1034:4: (otherlv_3= RULE_ID )
            // InternalCalur.g:1035:5: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getImportStatementRule());
              					}
              				
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_3, grammarAccess.getImportStatementAccess().getCapsuleNameClassCrossReference_2_0());
              				
            }

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getImportStatementAccess().getAtKeyword_3());
              		
            }
            // InternalCalur.g:1050:3: ( (lv_partRef_5_0= rulePartRef ) )
            // InternalCalur.g:1051:4: (lv_partRef_5_0= rulePartRef )
            {
            // InternalCalur.g:1051:4: (lv_partRef_5_0= rulePartRef )
            // InternalCalur.g:1052:5: lv_partRef_5_0= rulePartRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportStatementAccess().getPartRefPartRefParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_partRef_5_0=rulePartRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportStatementRule());
              					}
              					set(
              						current,
              						"partRef",
              						lv_partRef_5_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PartRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getImportStatementAccess().getSemicolonKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleDeportStatement"
    // InternalCalur.g:1077:1: entryRuleDeportStatement returns [EObject current=null] : iv_ruleDeportStatement= ruleDeportStatement EOF ;
    public final EObject entryRuleDeportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeportStatement = null;


        try {
            // InternalCalur.g:1077:56: (iv_ruleDeportStatement= ruleDeportStatement EOF )
            // InternalCalur.g:1078:2: iv_ruleDeportStatement= ruleDeportStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeportStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDeportStatement=ruleDeportStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeportStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeportStatement"


    // $ANTLR start "ruleDeportStatement"
    // InternalCalur.g:1084:1: ruleDeportStatement returns [EObject current=null] : (otherlv_0= 'deport' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' ) ;
    public final EObject ruleDeportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_partRef_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1090:2: ( (otherlv_0= 'deport' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' ) )
            // InternalCalur.g:1091:2: (otherlv_0= 'deport' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:1091:2: (otherlv_0= 'deport' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';' )
            // InternalCalur.g:1092:3: otherlv_0= 'deport' ( (lv_partRef_1_0= rulePartRef ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getDeportStatementAccess().getDeportKeyword_0());
              		
            }
            // InternalCalur.g:1096:3: ( (lv_partRef_1_0= rulePartRef ) )
            // InternalCalur.g:1097:4: (lv_partRef_1_0= rulePartRef )
            {
            // InternalCalur.g:1097:4: (lv_partRef_1_0= rulePartRef )
            // InternalCalur.g:1098:5: lv_partRef_1_0= rulePartRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDeportStatementAccess().getPartRefPartRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_partRef_1_0=rulePartRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDeportStatementRule());
              					}
              					set(
              						current,
              						"partRef",
              						lv_partRef_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PartRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDeportStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeportStatement"


    // $ANTLR start "entryRuleRegisterStatement"
    // InternalCalur.g:1123:1: entryRuleRegisterStatement returns [EObject current=null] : iv_ruleRegisterStatement= ruleRegisterStatement EOF ;
    public final EObject entryRuleRegisterStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRegisterStatement = null;


        try {
            // InternalCalur.g:1123:58: (iv_ruleRegisterStatement= ruleRegisterStatement EOF )
            // InternalCalur.g:1124:2: iv_ruleRegisterStatement= ruleRegisterStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRegisterStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRegisterStatement=ruleRegisterStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRegisterStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRegisterStatement"


    // $ANTLR start "ruleRegisterStatement"
    // InternalCalur.g:1130:1: ruleRegisterStatement returns [EObject current=null] : (otherlv_0= 'register' ( (lv_portSource_1_0= rulePortRef ) ) otherlv_2= 'to' ( (lv_portDest_3_0= rulePortRef ) ) otherlv_4= ';' ) ;
    public final EObject ruleRegisterStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_portSource_1_0 = null;

        EObject lv_portDest_3_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1136:2: ( (otherlv_0= 'register' ( (lv_portSource_1_0= rulePortRef ) ) otherlv_2= 'to' ( (lv_portDest_3_0= rulePortRef ) ) otherlv_4= ';' ) )
            // InternalCalur.g:1137:2: (otherlv_0= 'register' ( (lv_portSource_1_0= rulePortRef ) ) otherlv_2= 'to' ( (lv_portDest_3_0= rulePortRef ) ) otherlv_4= ';' )
            {
            // InternalCalur.g:1137:2: (otherlv_0= 'register' ( (lv_portSource_1_0= rulePortRef ) ) otherlv_2= 'to' ( (lv_portDest_3_0= rulePortRef ) ) otherlv_4= ';' )
            // InternalCalur.g:1138:3: otherlv_0= 'register' ( (lv_portSource_1_0= rulePortRef ) ) otherlv_2= 'to' ( (lv_portDest_3_0= rulePortRef ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRegisterStatementAccess().getRegisterKeyword_0());
              		
            }
            // InternalCalur.g:1142:3: ( (lv_portSource_1_0= rulePortRef ) )
            // InternalCalur.g:1143:4: (lv_portSource_1_0= rulePortRef )
            {
            // InternalCalur.g:1143:4: (lv_portSource_1_0= rulePortRef )
            // InternalCalur.g:1144:5: lv_portSource_1_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRegisterStatementAccess().getPortSourcePortRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_portSource_1_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRegisterStatementRule());
              					}
              					set(
              						current,
              						"portSource",
              						lv_portSource_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getRegisterStatementAccess().getToKeyword_2());
              		
            }
            // InternalCalur.g:1165:3: ( (lv_portDest_3_0= rulePortRef ) )
            // InternalCalur.g:1166:4: (lv_portDest_3_0= rulePortRef )
            {
            // InternalCalur.g:1166:4: (lv_portDest_3_0= rulePortRef )
            // InternalCalur.g:1167:5: lv_portDest_3_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRegisterStatementAccess().getPortDestPortRefParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_portDest_3_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRegisterStatementRule());
              					}
              					set(
              						current,
              						"portDest",
              						lv_portDest_3_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRegisterStatementAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRegisterStatement"


    // $ANTLR start "entryRuleDeregisterStatement"
    // InternalCalur.g:1192:1: entryRuleDeregisterStatement returns [EObject current=null] : iv_ruleDeregisterStatement= ruleDeregisterStatement EOF ;
    public final EObject entryRuleDeregisterStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeregisterStatement = null;


        try {
            // InternalCalur.g:1192:60: (iv_ruleDeregisterStatement= ruleDeregisterStatement EOF )
            // InternalCalur.g:1193:2: iv_ruleDeregisterStatement= ruleDeregisterStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeregisterStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDeregisterStatement=ruleDeregisterStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeregisterStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeregisterStatement"


    // $ANTLR start "ruleDeregisterStatement"
    // InternalCalur.g:1199:1: ruleDeregisterStatement returns [EObject current=null] : (otherlv_0= 'deregister' ( (lv_port_1_0= rulePortRef ) ) otherlv_2= ';' ) ;
    public final EObject ruleDeregisterStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_port_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1205:2: ( (otherlv_0= 'deregister' ( (lv_port_1_0= rulePortRef ) ) otherlv_2= ';' ) )
            // InternalCalur.g:1206:2: (otherlv_0= 'deregister' ( (lv_port_1_0= rulePortRef ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:1206:2: (otherlv_0= 'deregister' ( (lv_port_1_0= rulePortRef ) ) otherlv_2= ';' )
            // InternalCalur.g:1207:3: otherlv_0= 'deregister' ( (lv_port_1_0= rulePortRef ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getDeregisterStatementAccess().getDeregisterKeyword_0());
              		
            }
            // InternalCalur.g:1211:3: ( (lv_port_1_0= rulePortRef ) )
            // InternalCalur.g:1212:4: (lv_port_1_0= rulePortRef )
            {
            // InternalCalur.g:1212:4: (lv_port_1_0= rulePortRef )
            // InternalCalur.g:1213:5: lv_port_1_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDeregisterStatementAccess().getPortPortRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_port_1_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDeregisterStatementRule());
              					}
              					set(
              						current,
              						"port",
              						lv_port_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDeregisterStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeregisterStatement"


    // $ANTLR start "entryRuleExpr"
    // InternalCalur.g:1238:1: entryRuleExpr returns [EObject current=null] : iv_ruleExpr= ruleExpr EOF ;
    public final EObject entryRuleExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpr = null;


        try {
            // InternalCalur.g:1238:45: (iv_ruleExpr= ruleExpr EOF )
            // InternalCalur.g:1239:2: iv_ruleExpr= ruleExpr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExprRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExpr=ruleExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpr; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpr"


    // $ANTLR start "ruleExpr"
    // InternalCalur.g:1245:1: ruleExpr returns [EObject current=null] : (this_ValueSpecification_0= ruleValueSpecification | this_VariableRef_1= ruleVariableRef | this_UnaryExpr_2= ruleUnaryExpr | this_ZeroaryExpr_3= ruleZeroaryExpr | (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' ) ) ;
    public final EObject ruleExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject this_ValueSpecification_0 = null;

        EObject this_VariableRef_1 = null;

        EObject this_UnaryExpr_2 = null;

        EObject this_ZeroaryExpr_3 = null;

        EObject this_BinaryExpr_5 = null;



        	enterRule();

        try {
            // InternalCalur.g:1251:2: ( (this_ValueSpecification_0= ruleValueSpecification | this_VariableRef_1= ruleVariableRef | this_UnaryExpr_2= ruleUnaryExpr | this_ZeroaryExpr_3= ruleZeroaryExpr | (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' ) ) )
            // InternalCalur.g:1252:2: (this_ValueSpecification_0= ruleValueSpecification | this_VariableRef_1= ruleVariableRef | this_UnaryExpr_2= ruleUnaryExpr | this_ZeroaryExpr_3= ruleZeroaryExpr | (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' ) )
            {
            // InternalCalur.g:1252:2: (this_ValueSpecification_0= ruleValueSpecification | this_VariableRef_1= ruleVariableRef | this_UnaryExpr_2= ruleUnaryExpr | this_ZeroaryExpr_3= ruleZeroaryExpr | (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' ) )
            int alt18=5;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
            case RULE_FLOAT:
            case 69:
            case 70:
            case 71:
                {
                alt18=1;
                }
                break;
            case RULE_ID:
            case 41:
                {
                alt18=2;
                }
                break;
            case 49:
            case 56:
            case 57:
            case 58:
            case 59:
                {
                alt18=3;
                }
                break;
            case 60:
            case 61:
            case 62:
            case 63:
                {
                alt18=4;
                }
                break;
            case 34:
                {
                alt18=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalCalur.g:1253:3: this_ValueSpecification_0= ruleValueSpecification
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExprAccess().getValueSpecificationParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ValueSpecification_0=ruleValueSpecification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ValueSpecification_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:1262:3: this_VariableRef_1= ruleVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExprAccess().getVariableRefParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_VariableRef_1=ruleVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_VariableRef_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:1271:3: this_UnaryExpr_2= ruleUnaryExpr
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExprAccess().getUnaryExprParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnaryExpr_2=ruleUnaryExpr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryExpr_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:1280:3: this_ZeroaryExpr_3= ruleZeroaryExpr
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExprAccess().getZeroaryExprParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ZeroaryExpr_3=ruleZeroaryExpr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ZeroaryExpr_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalCalur.g:1289:3: (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' )
                    {
                    // InternalCalur.g:1289:3: (otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')' )
                    // InternalCalur.g:1290:4: otherlv_4= '(' this_BinaryExpr_5= ruleBinaryExpr otherlv_6= ')'
                    {
                    otherlv_4=(Token)match(input,34,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getExprAccess().getLeftParenthesisKeyword_4_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getExprAccess().getBinaryExprParserRuleCall_4_1());
                      			
                    }
                    pushFollow(FOLLOW_21);
                    this_BinaryExpr_5=ruleBinaryExpr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_BinaryExpr_5;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    otherlv_6=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getExprAccess().getRightParenthesisKeyword_4_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpr"


    // $ANTLR start "entryRuleVariableInitializationStatement"
    // InternalCalur.g:1311:1: entryRuleVariableInitializationStatement returns [EObject current=null] : iv_ruleVariableInitializationStatement= ruleVariableInitializationStatement EOF ;
    public final EObject entryRuleVariableInitializationStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableInitializationStatement = null;


        try {
            // InternalCalur.g:1311:72: (iv_ruleVariableInitializationStatement= ruleVariableInitializationStatement EOF )
            // InternalCalur.g:1312:2: iv_ruleVariableInitializationStatement= ruleVariableInitializationStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableInitializationStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariableInitializationStatement=ruleVariableInitializationStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableInitializationStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableInitializationStatement"


    // $ANTLR start "ruleVariableInitializationStatement"
    // InternalCalur.g:1318:1: ruleVariableInitializationStatement returns [EObject current=null] : (otherlv_0= 'var' ( (lv_localVar_1_0= ruleLocalVariable ) ) otherlv_2= ';' ) ;
    public final EObject ruleVariableInitializationStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_localVar_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1324:2: ( (otherlv_0= 'var' ( (lv_localVar_1_0= ruleLocalVariable ) ) otherlv_2= ';' ) )
            // InternalCalur.g:1325:2: (otherlv_0= 'var' ( (lv_localVar_1_0= ruleLocalVariable ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:1325:2: (otherlv_0= 'var' ( (lv_localVar_1_0= ruleLocalVariable ) ) otherlv_2= ';' )
            // InternalCalur.g:1326:3: otherlv_0= 'var' ( (lv_localVar_1_0= ruleLocalVariable ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,36,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getVariableInitializationStatementAccess().getVarKeyword_0());
              		
            }
            // InternalCalur.g:1330:3: ( (lv_localVar_1_0= ruleLocalVariable ) )
            // InternalCalur.g:1331:4: (lv_localVar_1_0= ruleLocalVariable )
            {
            // InternalCalur.g:1331:4: (lv_localVar_1_0= ruleLocalVariable )
            // InternalCalur.g:1332:5: lv_localVar_1_0= ruleLocalVariable
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getVariableInitializationStatementAccess().getLocalVarLocalVariableParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_localVar_1_0=ruleLocalVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getVariableInitializationStatementRule());
              					}
              					set(
              						current,
              						"localVar",
              						lv_localVar_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.LocalVariable");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getVariableInitializationStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableInitializationStatement"


    // $ANTLR start "entryRuleLocalVariable"
    // InternalCalur.g:1357:1: entryRuleLocalVariable returns [EObject current=null] : iv_ruleLocalVariable= ruleLocalVariable EOF ;
    public final EObject entryRuleLocalVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLocalVariable = null;


        try {
            // InternalCalur.g:1357:54: (iv_ruleLocalVariable= ruleLocalVariable EOF )
            // InternalCalur.g:1358:2: iv_ruleLocalVariable= ruleLocalVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLocalVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLocalVariable=ruleLocalVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLocalVariable; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLocalVariable"


    // $ANTLR start "ruleLocalVariable"
    // InternalCalur.g:1364:1: ruleLocalVariable returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleLocalVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalCalur.g:1370:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalCalur.g:1371:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalCalur.g:1371:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) )
            // InternalCalur.g:1372:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) )
            {
            // InternalCalur.g:1372:3: ()
            // InternalCalur.g:1373:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLocalVariableAccess().getLocalVariableAction_0(),
              					current);
              			
            }

            }

            // InternalCalur.g:1379:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCalur.g:1380:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCalur.g:1380:4: (lv_name_1_0= RULE_ID )
            // InternalCalur.g:1381:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getLocalVariableAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLocalVariableRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getLocalVariableAccess().getColonKeyword_2());
              		
            }
            // InternalCalur.g:1401:3: ( (otherlv_3= RULE_ID ) )
            // InternalCalur.g:1402:4: (otherlv_3= RULE_ID )
            {
            // InternalCalur.g:1402:4: (otherlv_3= RULE_ID )
            // InternalCalur.g:1403:5: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLocalVariableRule());
              					}
              				
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_3, grammarAccess.getLocalVariableAccess().getTypeTypeCrossReference_3_0());
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLocalVariable"


    // $ANTLR start "entryRuleCallStatement"
    // InternalCalur.g:1418:1: entryRuleCallStatement returns [EObject current=null] : iv_ruleCallStatement= ruleCallStatement EOF ;
    public final EObject entryRuleCallStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallStatement = null;


        try {
            // InternalCalur.g:1418:54: (iv_ruleCallStatement= ruleCallStatement EOF )
            // InternalCalur.g:1419:2: iv_ruleCallStatement= ruleCallStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallStatement=ruleCallStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallStatement"


    // $ANTLR start "ruleCallStatement"
    // InternalCalur.g:1425:1: ruleCallStatement returns [EObject current=null] : (otherlv_0= 'call' ( (lv_varRef_1_0= ruleVariableRef ) ) otherlv_2= ';' ) ;
    public final EObject ruleCallStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_varRef_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1431:2: ( (otherlv_0= 'call' ( (lv_varRef_1_0= ruleVariableRef ) ) otherlv_2= ';' ) )
            // InternalCalur.g:1432:2: (otherlv_0= 'call' ( (lv_varRef_1_0= ruleVariableRef ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:1432:2: (otherlv_0= 'call' ( (lv_varRef_1_0= ruleVariableRef ) ) otherlv_2= ';' )
            // InternalCalur.g:1433:3: otherlv_0= 'call' ( (lv_varRef_1_0= ruleVariableRef ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCallStatementAccess().getCallKeyword_0());
              		
            }
            // InternalCalur.g:1437:3: ( (lv_varRef_1_0= ruleVariableRef ) )
            // InternalCalur.g:1438:4: (lv_varRef_1_0= ruleVariableRef )
            {
            // InternalCalur.g:1438:4: (lv_varRef_1_0= ruleVariableRef )
            // InternalCalur.g:1439:5: lv_varRef_1_0= ruleVariableRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallStatementAccess().getVarRefVariableRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_varRef_1_0=ruleVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCallStatementRule());
              					}
              					set(
              						current,
              						"varRef",
              						lv_varRef_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.VariableRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getCallStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallStatement"


    // $ANTLR start "entryRuleReturnStatement"
    // InternalCalur.g:1464:1: entryRuleReturnStatement returns [EObject current=null] : iv_ruleReturnStatement= ruleReturnStatement EOF ;
    public final EObject entryRuleReturnStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnStatement = null;


        try {
            // InternalCalur.g:1464:56: (iv_ruleReturnStatement= ruleReturnStatement EOF )
            // InternalCalur.g:1465:2: iv_ruleReturnStatement= ruleReturnStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReturnStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReturnStatement=ruleReturnStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReturnStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnStatement"


    // $ANTLR start "ruleReturnStatement"
    // InternalCalur.g:1471:1: ruleReturnStatement returns [EObject current=null] : (otherlv_0= 'return' ( (lv_expre_1_0= ruleExpr ) ) otherlv_2= ';' ) ;
    public final EObject ruleReturnStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_expre_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1477:2: ( (otherlv_0= 'return' ( (lv_expre_1_0= ruleExpr ) ) otherlv_2= ';' ) )
            // InternalCalur.g:1478:2: (otherlv_0= 'return' ( (lv_expre_1_0= ruleExpr ) ) otherlv_2= ';' )
            {
            // InternalCalur.g:1478:2: (otherlv_0= 'return' ( (lv_expre_1_0= ruleExpr ) ) otherlv_2= ';' )
            // InternalCalur.g:1479:3: otherlv_0= 'return' ( (lv_expre_1_0= ruleExpr ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,38,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getReturnStatementAccess().getReturnKeyword_0());
              		
            }
            // InternalCalur.g:1483:3: ( (lv_expre_1_0= ruleExpr ) )
            // InternalCalur.g:1484:4: (lv_expre_1_0= ruleExpr )
            {
            // InternalCalur.g:1484:4: (lv_expre_1_0= ruleExpr )
            // InternalCalur.g:1485:5: lv_expre_1_0= ruleExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getReturnStatementAccess().getExpreExprParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_expre_1_0=ruleExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getReturnStatementRule());
              					}
              					set(
              						current,
              						"expre",
              						lv_expre_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Expr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getReturnStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnStatement"


    // $ANTLR start "entryRuleSrandStatement"
    // InternalCalur.g:1510:1: entryRuleSrandStatement returns [EObject current=null] : iv_ruleSrandStatement= ruleSrandStatement EOF ;
    public final EObject entryRuleSrandStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSrandStatement = null;


        try {
            // InternalCalur.g:1510:55: (iv_ruleSrandStatement= ruleSrandStatement EOF )
            // InternalCalur.g:1511:2: iv_ruleSrandStatement= ruleSrandStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSrandStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSrandStatement=ruleSrandStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSrandStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSrandStatement"


    // $ANTLR start "ruleSrandStatement"
    // InternalCalur.g:1517:1: ruleSrandStatement returns [EObject current=null] : (otherlv_0= 'srand' otherlv_1= '(' ( (lv_seed_2_0= ruleBinaryExpr ) ) otherlv_3= ')' otherlv_4= ';' ) ;
    public final EObject ruleSrandStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_seed_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1523:2: ( (otherlv_0= 'srand' otherlv_1= '(' ( (lv_seed_2_0= ruleBinaryExpr ) ) otherlv_3= ')' otherlv_4= ';' ) )
            // InternalCalur.g:1524:2: (otherlv_0= 'srand' otherlv_1= '(' ( (lv_seed_2_0= ruleBinaryExpr ) ) otherlv_3= ')' otherlv_4= ';' )
            {
            // InternalCalur.g:1524:2: (otherlv_0= 'srand' otherlv_1= '(' ( (lv_seed_2_0= ruleBinaryExpr ) ) otherlv_3= ')' otherlv_4= ';' )
            // InternalCalur.g:1525:3: otherlv_0= 'srand' otherlv_1= '(' ( (lv_seed_2_0= ruleBinaryExpr ) ) otherlv_3= ')' otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,39,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSrandStatementAccess().getSrandKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,34,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSrandStatementAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalCalur.g:1533:3: ( (lv_seed_2_0= ruleBinaryExpr ) )
            // InternalCalur.g:1534:4: (lv_seed_2_0= ruleBinaryExpr )
            {
            // InternalCalur.g:1534:4: (lv_seed_2_0= ruleBinaryExpr )
            // InternalCalur.g:1535:5: lv_seed_2_0= ruleBinaryExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSrandStatementAccess().getSeedBinaryExprParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_21);
            lv_seed_2_0=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSrandStatementRule());
              					}
              					set(
              						current,
              						"seed",
              						lv_seed_2_0,
              						"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,35,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getSrandStatementAccess().getRightParenthesisKeyword_3());
              		
            }
            otherlv_4=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSrandStatementAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSrandStatement"


    // $ANTLR start "entryRuleVariableAffectationStatement"
    // InternalCalur.g:1564:1: entryRuleVariableAffectationStatement returns [EObject current=null] : iv_ruleVariableAffectationStatement= ruleVariableAffectationStatement EOF ;
    public final EObject entryRuleVariableAffectationStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAffectationStatement = null;


        try {
            // InternalCalur.g:1564:69: (iv_ruleVariableAffectationStatement= ruleVariableAffectationStatement EOF )
            // InternalCalur.g:1565:2: iv_ruleVariableAffectationStatement= ruleVariableAffectationStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAffectationStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariableAffectationStatement=ruleVariableAffectationStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAffectationStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAffectationStatement"


    // $ANTLR start "ruleVariableAffectationStatement"
    // InternalCalur.g:1571:1: ruleVariableAffectationStatement returns [EObject current=null] : ( ( (lv_varRef_0_0= ruleVariableRef ) ) otherlv_1= ':=' ( (lv_expr_2_0= ruleBinaryExpr ) ) otherlv_3= ';' ) ;
    public final EObject ruleVariableAffectationStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_varRef_0_0 = null;

        EObject lv_expr_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1577:2: ( ( ( (lv_varRef_0_0= ruleVariableRef ) ) otherlv_1= ':=' ( (lv_expr_2_0= ruleBinaryExpr ) ) otherlv_3= ';' ) )
            // InternalCalur.g:1578:2: ( ( (lv_varRef_0_0= ruleVariableRef ) ) otherlv_1= ':=' ( (lv_expr_2_0= ruleBinaryExpr ) ) otherlv_3= ';' )
            {
            // InternalCalur.g:1578:2: ( ( (lv_varRef_0_0= ruleVariableRef ) ) otherlv_1= ':=' ( (lv_expr_2_0= ruleBinaryExpr ) ) otherlv_3= ';' )
            // InternalCalur.g:1579:3: ( (lv_varRef_0_0= ruleVariableRef ) ) otherlv_1= ':=' ( (lv_expr_2_0= ruleBinaryExpr ) ) otherlv_3= ';'
            {
            // InternalCalur.g:1579:3: ( (lv_varRef_0_0= ruleVariableRef ) )
            // InternalCalur.g:1580:4: (lv_varRef_0_0= ruleVariableRef )
            {
            // InternalCalur.g:1580:4: (lv_varRef_0_0= ruleVariableRef )
            // InternalCalur.g:1581:5: lv_varRef_0_0= ruleVariableRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getVariableAffectationStatementAccess().getVarRefVariableRefParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_24);
            lv_varRef_0_0=ruleVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getVariableAffectationStatementRule());
              					}
              					set(
              						current,
              						"varRef",
              						lv_varRef_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.VariableRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,40,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getVariableAffectationStatementAccess().getColonEqualsSignKeyword_1());
              		
            }
            // InternalCalur.g:1602:3: ( (lv_expr_2_0= ruleBinaryExpr ) )
            // InternalCalur.g:1603:4: (lv_expr_2_0= ruleBinaryExpr )
            {
            // InternalCalur.g:1603:4: (lv_expr_2_0= ruleBinaryExpr )
            // InternalCalur.g:1604:5: lv_expr_2_0= ruleBinaryExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getVariableAffectationStatementAccess().getExprBinaryExprParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_expr_2_0=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getVariableAffectationStatementRule());
              					}
              					set(
              						current,
              						"expr",
              						lv_expr_2_0,
              						"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getVariableAffectationStatementAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAffectationStatement"


    // $ANTLR start "entryRuleVariableRef"
    // InternalCalur.g:1629:1: entryRuleVariableRef returns [EObject current=null] : iv_ruleVariableRef= ruleVariableRef EOF ;
    public final EObject entryRuleVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableRef = null;


        try {
            // InternalCalur.g:1629:52: (iv_ruleVariableRef= ruleVariableRef EOF )
            // InternalCalur.g:1630:2: iv_ruleVariableRef= ruleVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariableRef=ruleVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableRef"


    // $ANTLR start "ruleVariableRef"
    // InternalCalur.g:1636:1: ruleVariableRef returns [EObject current=null] : (this_ProtocolVariableRef_0= ruleProtocolVariableRef | this_GlobalVariableRef_1= ruleGlobalVariableRef ) ;
    public final EObject ruleVariableRef() throws RecognitionException {
        EObject current = null;

        EObject this_ProtocolVariableRef_0 = null;

        EObject this_GlobalVariableRef_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:1642:2: ( (this_ProtocolVariableRef_0= ruleProtocolVariableRef | this_GlobalVariableRef_1= ruleGlobalVariableRef ) )
            // InternalCalur.g:1643:2: (this_ProtocolVariableRef_0= ruleProtocolVariableRef | this_GlobalVariableRef_1= ruleGlobalVariableRef )
            {
            // InternalCalur.g:1643:2: (this_ProtocolVariableRef_0= ruleProtocolVariableRef | this_GlobalVariableRef_1= ruleGlobalVariableRef )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_ID) ) {
                alt19=1;
            }
            else if ( (LA19_0==41) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalCalur.g:1644:3: this_ProtocolVariableRef_0= ruleProtocolVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getVariableRefAccess().getProtocolVariableRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ProtocolVariableRef_0=ruleProtocolVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ProtocolVariableRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:1653:3: this_GlobalVariableRef_1= ruleGlobalVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getVariableRefAccess().getGlobalVariableRefParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GlobalVariableRef_1=ruleGlobalVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GlobalVariableRef_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableRef"


    // $ANTLR start "entryRuleGlobalVariableRef"
    // InternalCalur.g:1665:1: entryRuleGlobalVariableRef returns [EObject current=null] : iv_ruleGlobalVariableRef= ruleGlobalVariableRef EOF ;
    public final EObject entryRuleGlobalVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGlobalVariableRef = null;


        try {
            // InternalCalur.g:1665:58: (iv_ruleGlobalVariableRef= ruleGlobalVariableRef EOF )
            // InternalCalur.g:1666:2: iv_ruleGlobalVariableRef= ruleGlobalVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGlobalVariableRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGlobalVariableRef=ruleGlobalVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGlobalVariableRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGlobalVariableRef"


    // $ANTLR start "ruleGlobalVariableRef"
    // InternalCalur.g:1672:1: ruleGlobalVariableRef returns [EObject current=null] : (otherlv_0= 'this.' ( (lv_segments_1_0= ruleField ) ) (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )* ) ;
    public final EObject ruleGlobalVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_segments_1_0 = null;

        EObject lv_segments_3_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1678:2: ( (otherlv_0= 'this.' ( (lv_segments_1_0= ruleField ) ) (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )* ) )
            // InternalCalur.g:1679:2: (otherlv_0= 'this.' ( (lv_segments_1_0= ruleField ) ) (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )* )
            {
            // InternalCalur.g:1679:2: (otherlv_0= 'this.' ( (lv_segments_1_0= ruleField ) ) (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )* )
            // InternalCalur.g:1680:3: otherlv_0= 'this.' ( (lv_segments_1_0= ruleField ) ) (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )*
            {
            otherlv_0=(Token)match(input,41,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getGlobalVariableRefAccess().getThisKeyword_0());
              		
            }
            // InternalCalur.g:1684:3: ( (lv_segments_1_0= ruleField ) )
            // InternalCalur.g:1685:4: (lv_segments_1_0= ruleField )
            {
            // InternalCalur.g:1685:4: (lv_segments_1_0= ruleField )
            // InternalCalur.g:1686:5: lv_segments_1_0= ruleField
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGlobalVariableRefAccess().getSegmentsFieldParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_25);
            lv_segments_1_0=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getGlobalVariableRefRule());
              					}
              					add(
              						current,
              						"segments",
              						lv_segments_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Field");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:1703:3: (otherlv_2= '.' ( (lv_segments_3_0= ruleField ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==42) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalCalur.g:1704:4: otherlv_2= '.' ( (lv_segments_3_0= ruleField ) )
            	    {
            	    otherlv_2=(Token)match(input,42,FOLLOW_7); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getGlobalVariableRefAccess().getFullStopKeyword_2_0());
            	      			
            	    }
            	    // InternalCalur.g:1708:4: ( (lv_segments_3_0= ruleField ) )
            	    // InternalCalur.g:1709:5: (lv_segments_3_0= ruleField )
            	    {
            	    // InternalCalur.g:1709:5: (lv_segments_3_0= ruleField )
            	    // InternalCalur.g:1710:6: lv_segments_3_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGlobalVariableRefAccess().getSegmentsFieldParserRuleCall_2_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_25);
            	    lv_segments_3_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGlobalVariableRefRule());
            	      						}
            	      						add(
            	      							current,
            	      							"segments",
            	      							lv_segments_3_0,
            	      							"ca.queensu.cs.umlrt.calur.Calur.Field");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGlobalVariableRef"


    // $ANTLR start "entryRuleProtocolVariableRef"
    // InternalCalur.g:1732:1: entryRuleProtocolVariableRef returns [EObject current=null] : iv_ruleProtocolVariableRef= ruleProtocolVariableRef EOF ;
    public final EObject entryRuleProtocolVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProtocolVariableRef = null;


        try {
            // InternalCalur.g:1732:60: (iv_ruleProtocolVariableRef= ruleProtocolVariableRef EOF )
            // InternalCalur.g:1733:2: iv_ruleProtocolVariableRef= ruleProtocolVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProtocolVariableRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleProtocolVariableRef=ruleProtocolVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProtocolVariableRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProtocolVariableRef"


    // $ANTLR start "ruleProtocolVariableRef"
    // InternalCalur.g:1739:1: ruleProtocolVariableRef returns [EObject current=null] : ( ( (lv_segments_0_0= ruleField ) ) (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )* ) ;
    public final EObject ruleProtocolVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_segments_0_0 = null;

        EObject lv_segments_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1745:2: ( ( ( (lv_segments_0_0= ruleField ) ) (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )* ) )
            // InternalCalur.g:1746:2: ( ( (lv_segments_0_0= ruleField ) ) (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )* )
            {
            // InternalCalur.g:1746:2: ( ( (lv_segments_0_0= ruleField ) ) (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )* )
            // InternalCalur.g:1747:3: ( (lv_segments_0_0= ruleField ) ) (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )*
            {
            // InternalCalur.g:1747:3: ( (lv_segments_0_0= ruleField ) )
            // InternalCalur.g:1748:4: (lv_segments_0_0= ruleField )
            {
            // InternalCalur.g:1748:4: (lv_segments_0_0= ruleField )
            // InternalCalur.g:1749:5: lv_segments_0_0= ruleField
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getProtocolVariableRefAccess().getSegmentsFieldParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_25);
            lv_segments_0_0=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getProtocolVariableRefRule());
              					}
              					add(
              						current,
              						"segments",
              						lv_segments_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Field");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:1766:3: (otherlv_1= '.' ( (lv_segments_2_0= ruleField ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==42) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalCalur.g:1767:4: otherlv_1= '.' ( (lv_segments_2_0= ruleField ) )
            	    {
            	    otherlv_1=(Token)match(input,42,FOLLOW_7); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getProtocolVariableRefAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    // InternalCalur.g:1771:4: ( (lv_segments_2_0= ruleField ) )
            	    // InternalCalur.g:1772:5: (lv_segments_2_0= ruleField )
            	    {
            	    // InternalCalur.g:1772:5: (lv_segments_2_0= ruleField )
            	    // InternalCalur.g:1773:6: lv_segments_2_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getProtocolVariableRefAccess().getSegmentsFieldParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_25);
            	    lv_segments_2_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getProtocolVariableRefRule());
            	      						}
            	      						add(
            	      							current,
            	      							"segments",
            	      							lv_segments_2_0,
            	      							"ca.queensu.cs.umlrt.calur.Calur.Field");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProtocolVariableRef"


    // $ANTLR start "entryRuleField"
    // InternalCalur.g:1795:1: entryRuleField returns [EObject current=null] : iv_ruleField= ruleField EOF ;
    public final EObject entryRuleField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleField = null;


        try {
            // InternalCalur.g:1795:46: (iv_ruleField= ruleField EOF )
            // InternalCalur.g:1796:2: iv_ruleField= ruleField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleField=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleField; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // InternalCalur.g:1802:1: ruleField returns [EObject current=null] : (this_Property_0= ruleProperty | this_Operation_1= ruleOperation ) ;
    public final EObject ruleField() throws RecognitionException {
        EObject current = null;

        EObject this_Property_0 = null;

        EObject this_Operation_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:1808:2: ( (this_Property_0= ruleProperty | this_Operation_1= ruleOperation ) )
            // InternalCalur.g:1809:2: (this_Property_0= ruleProperty | this_Operation_1= ruleOperation )
            {
            // InternalCalur.g:1809:2: (this_Property_0= ruleProperty | this_Operation_1= ruleOperation )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==RULE_ID) ) {
                int LA22_1 = input.LA(2);

                if ( (LA22_1==EOF||LA22_1==13||LA22_1==24||LA22_1==35||LA22_1==40||LA22_1==42||LA22_1==44||(LA22_1>=50 && LA22_1<=55)||(LA22_1>=64 && LA22_1<=68)||(LA22_1>=72 && LA22_1<=74)) ) {
                    alt22=1;
                }
                else if ( (LA22_1==34) ) {
                    alt22=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 22, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalCalur.g:1810:3: this_Property_0= ruleProperty
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFieldAccess().getPropertyParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Property_0=ruleProperty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Property_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:1819:3: this_Operation_1= ruleOperation
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFieldAccess().getOperationParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Operation_1=ruleOperation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Operation_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleProperty"
    // InternalCalur.g:1831:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalCalur.g:1831:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalCalur.g:1832:2: iv_ruleProperty= ruleProperty EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPropertyRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProperty; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalCalur.g:1838:1: ruleProperty returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalCalur.g:1844:2: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCalur.g:1845:2: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCalur.g:1845:2: ( (otherlv_0= RULE_ID ) )
            // InternalCalur.g:1846:3: (otherlv_0= RULE_ID )
            {
            // InternalCalur.g:1846:3: (otherlv_0= RULE_ID )
            // InternalCalur.g:1847:4: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getPropertyRule());
              				}
              			
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_0, grammarAccess.getPropertyAccess().getPropertyTypedElementCrossReference_0());
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleOperation"
    // InternalCalur.g:1861:1: entryRuleOperation returns [EObject current=null] : iv_ruleOperation= ruleOperation EOF ;
    public final EObject entryRuleOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperation = null;


        try {
            // InternalCalur.g:1861:50: (iv_ruleOperation= ruleOperation EOF )
            // InternalCalur.g:1862:2: iv_ruleOperation= ruleOperation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOperationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOperation=ruleOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOperation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // InternalCalur.g:1868:1: ruleOperation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_arguments_2_0= ruleArguments ) )? otherlv_3= ')' ) ;
    public final EObject ruleOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_arguments_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1874:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_arguments_2_0= ruleArguments ) )? otherlv_3= ')' ) )
            // InternalCalur.g:1875:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_arguments_2_0= ruleArguments ) )? otherlv_3= ')' )
            {
            // InternalCalur.g:1875:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_arguments_2_0= ruleArguments ) )? otherlv_3= ')' )
            // InternalCalur.g:1876:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_arguments_2_0= ruleArguments ) )? otherlv_3= ')'
            {
            // InternalCalur.g:1876:3: ( (otherlv_0= RULE_ID ) )
            // InternalCalur.g:1877:4: (otherlv_0= RULE_ID )
            {
            // InternalCalur.g:1877:4: (otherlv_0= RULE_ID )
            // InternalCalur.g:1878:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getOperationRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getOperationAccess().getOperationOperationCrossReference_0_0());
              				
            }

            }


            }

            otherlv_1=(Token)match(input,34,FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getOperationAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalCalur.g:1893:3: ( (lv_arguments_2_0= ruleArguments ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=RULE_STRING && LA23_0<=RULE_FLOAT)||LA23_0==34||LA23_0==41||LA23_0==49||(LA23_0>=56 && LA23_0<=63)||(LA23_0>=69 && LA23_0<=71)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalCalur.g:1894:4: (lv_arguments_2_0= ruleArguments )
                    {
                    // InternalCalur.g:1894:4: (lv_arguments_2_0= ruleArguments )
                    // InternalCalur.g:1895:5: lv_arguments_2_0= ruleArguments
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getOperationAccess().getArgumentsArgumentsParserRuleCall_2_0());
                      				
                    }
                    pushFollow(FOLLOW_21);
                    lv_arguments_2_0=ruleArguments();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getOperationRule());
                      					}
                      					set(
                      						current,
                      						"arguments",
                      						lv_arguments_2_0,
                      						"ca.queensu.cs.umlrt.calur.Calur.Arguments");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getOperationAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleArguments"
    // InternalCalur.g:1920:1: entryRuleArguments returns [EObject current=null] : iv_ruleArguments= ruleArguments EOF ;
    public final EObject entryRuleArguments() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArguments = null;


        try {
            // InternalCalur.g:1920:50: (iv_ruleArguments= ruleArguments EOF )
            // InternalCalur.g:1921:2: iv_ruleArguments= ruleArguments EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArgumentsRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleArguments=ruleArguments();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArguments; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArguments"


    // $ANTLR start "ruleArguments"
    // InternalCalur.g:1927:1: ruleArguments returns [EObject current=null] : ( ( (lv_arguments_0_0= ruleBinaryExpr ) ) (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )* ) ;
    public final EObject ruleArguments() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_arguments_0_0 = null;

        EObject lv_arguments_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:1933:2: ( ( ( (lv_arguments_0_0= ruleBinaryExpr ) ) (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )* ) )
            // InternalCalur.g:1934:2: ( ( (lv_arguments_0_0= ruleBinaryExpr ) ) (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )* )
            {
            // InternalCalur.g:1934:2: ( ( (lv_arguments_0_0= ruleBinaryExpr ) ) (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )* )
            // InternalCalur.g:1935:3: ( (lv_arguments_0_0= ruleBinaryExpr ) ) (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )*
            {
            // InternalCalur.g:1935:3: ( (lv_arguments_0_0= ruleBinaryExpr ) )
            // InternalCalur.g:1936:4: (lv_arguments_0_0= ruleBinaryExpr )
            {
            // InternalCalur.g:1936:4: (lv_arguments_0_0= ruleBinaryExpr )
            // InternalCalur.g:1937:5: lv_arguments_0_0= ruleBinaryExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getArgumentsAccess().getArgumentsBinaryExprParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_27);
            lv_arguments_0_0=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getArgumentsRule());
              					}
              					add(
              						current,
              						"arguments",
              						lv_arguments_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:1954:3: (otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==24) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalCalur.g:1955:4: otherlv_1= 'and' ( (lv_arguments_2_0= ruleBinaryExpr ) )
            	    {
            	    otherlv_1=(Token)match(input,24,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getArgumentsAccess().getAndKeyword_1_0());
            	      			
            	    }
            	    // InternalCalur.g:1959:4: ( (lv_arguments_2_0= ruleBinaryExpr ) )
            	    // InternalCalur.g:1960:5: (lv_arguments_2_0= ruleBinaryExpr )
            	    {
            	    // InternalCalur.g:1960:5: (lv_arguments_2_0= ruleBinaryExpr )
            	    // InternalCalur.g:1961:6: lv_arguments_2_0= ruleBinaryExpr
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getArgumentsAccess().getArgumentsBinaryExprParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_27);
            	    lv_arguments_2_0=ruleBinaryExpr();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getArgumentsRule());
            	      						}
            	      						add(
            	      							current,
            	      							"arguments",
            	      							lv_arguments_2_0,
            	      							"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArguments"


    // $ANTLR start "entryRuleObjectCallOperation"
    // InternalCalur.g:1983:1: entryRuleObjectCallOperation returns [EObject current=null] : iv_ruleObjectCallOperation= ruleObjectCallOperation EOF ;
    public final EObject entryRuleObjectCallOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectCallOperation = null;


        try {
            // InternalCalur.g:1983:60: (iv_ruleObjectCallOperation= ruleObjectCallOperation EOF )
            // InternalCalur.g:1984:2: iv_ruleObjectCallOperation= ruleObjectCallOperation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getObjectCallOperationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleObjectCallOperation=ruleObjectCallOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleObjectCallOperation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectCallOperation"


    // $ANTLR start "ruleObjectCallOperation"
    // InternalCalur.g:1990:1: ruleObjectCallOperation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) otherlv_3= ';' ) ;
    public final EObject ruleObjectCallOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalCalur.g:1996:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) otherlv_3= ';' ) )
            // InternalCalur.g:1997:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) otherlv_3= ';' )
            {
            // InternalCalur.g:1997:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) otherlv_3= ';' )
            // InternalCalur.g:1998:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) otherlv_3= ';'
            {
            // InternalCalur.g:1998:3: ( (otherlv_0= RULE_ID ) )
            // InternalCalur.g:1999:4: (otherlv_0= RULE_ID )
            {
            // InternalCalur.g:1999:4: (otherlv_0= RULE_ID )
            // InternalCalur.g:2000:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getObjectCallOperationRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getObjectCallOperationAccess().getVarPropertyCrossReference_0_0());
              				
            }

            }


            }

            otherlv_1=(Token)match(input,42,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getObjectCallOperationAccess().getFullStopKeyword_1());
              		
            }
            // InternalCalur.g:2015:3: ( (otherlv_2= RULE_ID ) )
            // InternalCalur.g:2016:4: (otherlv_2= RULE_ID )
            {
            // InternalCalur.g:2016:4: (otherlv_2= RULE_ID )
            // InternalCalur.g:2017:5: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getObjectCallOperationRule());
              					}
              				
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_2, grammarAccess.getObjectCallOperationAccess().getOperationOperationCrossReference_2_0());
              				
            }

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getObjectCallOperationAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectCallOperation"


    // $ANTLR start "entryRuleIfStatement"
    // InternalCalur.g:2036:1: entryRuleIfStatement returns [EObject current=null] : iv_ruleIfStatement= ruleIfStatement EOF ;
    public final EObject entryRuleIfStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfStatement = null;


        try {
            // InternalCalur.g:2036:52: (iv_ruleIfStatement= ruleIfStatement EOF )
            // InternalCalur.g:2037:2: iv_ruleIfStatement= ruleIfStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIfStatement=ruleIfStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfStatement"


    // $ANTLR start "ruleIfStatement"
    // InternalCalur.g:2043:1: ruleIfStatement returns [EObject current=null] : (otherlv_0= 'if' ( (lv_expr_1_0= ruleBinaryExpr ) ) otherlv_2= 'then' otherlv_3= '{' ( (lv_thenStatements_4_0= ruleStatement ) )+ otherlv_5= '}' (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )? ) ;
    public final EObject ruleIfStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_expr_1_0 = null;

        EObject lv_thenStatements_4_0 = null;

        EObject lv_elseStatements_8_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2049:2: ( (otherlv_0= 'if' ( (lv_expr_1_0= ruleBinaryExpr ) ) otherlv_2= 'then' otherlv_3= '{' ( (lv_thenStatements_4_0= ruleStatement ) )+ otherlv_5= '}' (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )? ) )
            // InternalCalur.g:2050:2: (otherlv_0= 'if' ( (lv_expr_1_0= ruleBinaryExpr ) ) otherlv_2= 'then' otherlv_3= '{' ( (lv_thenStatements_4_0= ruleStatement ) )+ otherlv_5= '}' (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )? )
            {
            // InternalCalur.g:2050:2: (otherlv_0= 'if' ( (lv_expr_1_0= ruleBinaryExpr ) ) otherlv_2= 'then' otherlv_3= '{' ( (lv_thenStatements_4_0= ruleStatement ) )+ otherlv_5= '}' (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )? )
            // InternalCalur.g:2051:3: otherlv_0= 'if' ( (lv_expr_1_0= ruleBinaryExpr ) ) otherlv_2= 'then' otherlv_3= '{' ( (lv_thenStatements_4_0= ruleStatement ) )+ otherlv_5= '}' (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )?
            {
            otherlv_0=(Token)match(input,43,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIfStatementAccess().getIfKeyword_0());
              		
            }
            // InternalCalur.g:2055:3: ( (lv_expr_1_0= ruleBinaryExpr ) )
            // InternalCalur.g:2056:4: (lv_expr_1_0= ruleBinaryExpr )
            {
            // InternalCalur.g:2056:4: (lv_expr_1_0= ruleBinaryExpr )
            // InternalCalur.g:2057:5: lv_expr_1_0= ruleBinaryExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIfStatementAccess().getExprBinaryExprParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_29);
            lv_expr_1_0=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIfStatementRule());
              					}
              					set(
              						current,
              						"expr",
              						lv_expr_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,44,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getIfStatementAccess().getThenKeyword_2());
              		
            }
            otherlv_3=(Token)match(input,45,FOLLOW_31); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalCalur.g:2082:3: ( (lv_thenStatements_4_0= ruleStatement ) )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_ID||LA25_0==12||LA25_0==14||LA25_0==20||LA25_0==22||(LA25_0>=27 && LA25_0<=33)||(LA25_0>=36 && LA25_0<=39)||LA25_0==41||LA25_0==43) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalCalur.g:2083:4: (lv_thenStatements_4_0= ruleStatement )
            	    {
            	    // InternalCalur.g:2083:4: (lv_thenStatements_4_0= ruleStatement )
            	    // InternalCalur.g:2084:5: lv_thenStatements_4_0= ruleStatement
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getIfStatementAccess().getThenStatementsStatementParserRuleCall_4_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_31);
            	    lv_thenStatements_4_0=ruleStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getIfStatementRule());
            	      					}
            	      					add(
            	      						current,
            	      						"thenStatements",
            	      						lv_thenStatements_4_0,
            	      						"ca.queensu.cs.umlrt.calur.Calur.Statement");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);

            otherlv_5=(Token)match(input,46,FOLLOW_32); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_5());
              		
            }
            // InternalCalur.g:2105:3: (otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==47) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalCalur.g:2106:4: otherlv_6= 'else' otherlv_7= '{' ( (lv_elseStatements_8_0= ruleStatement ) )+ otherlv_9= '}'
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getIfStatementAccess().getElseKeyword_6_0());
                      			
                    }
                    otherlv_7=(Token)match(input,45,FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_6_1());
                      			
                    }
                    // InternalCalur.g:2114:4: ( (lv_elseStatements_8_0= ruleStatement ) )+
                    int cnt26=0;
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==RULE_ID||LA26_0==12||LA26_0==14||LA26_0==20||LA26_0==22||(LA26_0>=27 && LA26_0<=33)||(LA26_0>=36 && LA26_0<=39)||LA26_0==41||LA26_0==43) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalCalur.g:2115:5: (lv_elseStatements_8_0= ruleStatement )
                    	    {
                    	    // InternalCalur.g:2115:5: (lv_elseStatements_8_0= ruleStatement )
                    	    // InternalCalur.g:2116:6: lv_elseStatements_8_0= ruleStatement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getIfStatementAccess().getElseStatementsStatementParserRuleCall_6_2_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_31);
                    	    lv_elseStatements_8_0=ruleStatement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getIfStatementRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"elseStatements",
                    	      							lv_elseStatements_8_0,
                    	      							"ca.queensu.cs.umlrt.calur.Calur.Statement");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt26 >= 1 ) break loop26;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(26, input);
                                throw eee;
                        }
                        cnt26++;
                    } while (true);

                    otherlv_9=(Token)match(input,46,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_6_3());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfStatement"


    // $ANTLR start "entryRuleUnaryExpr"
    // InternalCalur.g:2142:1: entryRuleUnaryExpr returns [EObject current=null] : iv_ruleUnaryExpr= ruleUnaryExpr EOF ;
    public final EObject entryRuleUnaryExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpr = null;


        try {
            // InternalCalur.g:2142:50: (iv_ruleUnaryExpr= ruleUnaryExpr EOF )
            // InternalCalur.g:2143:2: iv_ruleUnaryExpr= ruleUnaryExpr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryExprRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryExpr=ruleUnaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryExpr; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpr"


    // $ANTLR start "ruleUnaryExpr"
    // InternalCalur.g:2149:1: ruleUnaryExpr returns [EObject current=null] : ( ( (lv_operator_0_0= ruleUnaryOperator ) ) otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ) ;
    public final EObject ruleUnaryExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_operator_0_0 = null;

        EObject lv_expr_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2155:2: ( ( ( (lv_operator_0_0= ruleUnaryOperator ) ) otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ) )
            // InternalCalur.g:2156:2: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' )
            {
            // InternalCalur.g:2156:2: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' )
            // InternalCalur.g:2157:3: ( (lv_operator_0_0= ruleUnaryOperator ) ) otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')'
            {
            // InternalCalur.g:2157:3: ( (lv_operator_0_0= ruleUnaryOperator ) )
            // InternalCalur.g:2158:4: (lv_operator_0_0= ruleUnaryOperator )
            {
            // InternalCalur.g:2158:4: (lv_operator_0_0= ruleUnaryOperator )
            // InternalCalur.g:2159:5: lv_operator_0_0= ruleUnaryOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryExprAccess().getOperatorUnaryOperatorParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_23);
            lv_operator_0_0=ruleUnaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryExprRule());
              					}
              					set(
              						current,
              						"operator",
              						lv_operator_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.UnaryOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,34,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUnaryExprAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalCalur.g:2180:3: ( (lv_expr_2_0= ruleExpr ) )
            // InternalCalur.g:2181:4: (lv_expr_2_0= ruleExpr )
            {
            // InternalCalur.g:2181:4: (lv_expr_2_0= ruleExpr )
            // InternalCalur.g:2182:5: lv_expr_2_0= ruleExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryExprAccess().getExprExprParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_21);
            lv_expr_2_0=ruleExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryExprRule());
              					}
              					set(
              						current,
              						"expr",
              						lv_expr_2_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Expr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getUnaryExprAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpr"


    // $ANTLR start "entryRuleZeroaryExpr"
    // InternalCalur.g:2207:1: entryRuleZeroaryExpr returns [EObject current=null] : iv_ruleZeroaryExpr= ruleZeroaryExpr EOF ;
    public final EObject entryRuleZeroaryExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleZeroaryExpr = null;


        try {
            // InternalCalur.g:2207:52: (iv_ruleZeroaryExpr= ruleZeroaryExpr EOF )
            // InternalCalur.g:2208:2: iv_ruleZeroaryExpr= ruleZeroaryExpr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getZeroaryExprRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleZeroaryExpr=ruleZeroaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleZeroaryExpr; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZeroaryExpr"


    // $ANTLR start "ruleZeroaryExpr"
    // InternalCalur.g:2214:1: ruleZeroaryExpr returns [EObject current=null] : ( ( (lv_operator_0_0= ruleZeroaryOperator ) ) otherlv_1= '()' ) ;
    public final EObject ruleZeroaryExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_operator_0_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2220:2: ( ( ( (lv_operator_0_0= ruleZeroaryOperator ) ) otherlv_1= '()' ) )
            // InternalCalur.g:2221:2: ( ( (lv_operator_0_0= ruleZeroaryOperator ) ) otherlv_1= '()' )
            {
            // InternalCalur.g:2221:2: ( ( (lv_operator_0_0= ruleZeroaryOperator ) ) otherlv_1= '()' )
            // InternalCalur.g:2222:3: ( (lv_operator_0_0= ruleZeroaryOperator ) ) otherlv_1= '()'
            {
            // InternalCalur.g:2222:3: ( (lv_operator_0_0= ruleZeroaryOperator ) )
            // InternalCalur.g:2223:4: (lv_operator_0_0= ruleZeroaryOperator )
            {
            // InternalCalur.g:2223:4: (lv_operator_0_0= ruleZeroaryOperator )
            // InternalCalur.g:2224:5: lv_operator_0_0= ruleZeroaryOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getZeroaryExprAccess().getOperatorZeroaryOperatorParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_33);
            lv_operator_0_0=ruleZeroaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getZeroaryExprRule());
              					}
              					set(
              						current,
              						"operator",
              						lv_operator_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.ZeroaryOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,48,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getZeroaryExprAccess().getLeftParenthesisRightParenthesisKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZeroaryExpr"


    // $ANTLR start "entryRuleBinaryExpr"
    // InternalCalur.g:2249:1: entryRuleBinaryExpr returns [EObject current=null] : iv_ruleBinaryExpr= ruleBinaryExpr EOF ;
    public final EObject entryRuleBinaryExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryExpr = null;


        try {
            // InternalCalur.g:2249:51: (iv_ruleBinaryExpr= ruleBinaryExpr EOF )
            // InternalCalur.g:2250:2: iv_ruleBinaryExpr= ruleBinaryExpr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryExprRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryExpr=ruleBinaryExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryExpr; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryExpr"


    // $ANTLR start "ruleBinaryExpr"
    // InternalCalur.g:2256:1: ruleBinaryExpr returns [EObject current=null] : ( ( (lv_first_0_0= ruleExpr ) ) ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )? ) ;
    public final EObject ruleBinaryExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_first_0_0 = null;

        AntlrDatatypeRuleToken lv_operator_1_0 = null;

        EObject lv_last_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2262:2: ( ( ( (lv_first_0_0= ruleExpr ) ) ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )? ) )
            // InternalCalur.g:2263:2: ( ( (lv_first_0_0= ruleExpr ) ) ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )? )
            {
            // InternalCalur.g:2263:2: ( ( (lv_first_0_0= ruleExpr ) ) ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )? )
            // InternalCalur.g:2264:3: ( (lv_first_0_0= ruleExpr ) ) ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )?
            {
            // InternalCalur.g:2264:3: ( (lv_first_0_0= ruleExpr ) )
            // InternalCalur.g:2265:4: (lv_first_0_0= ruleExpr )
            {
            // InternalCalur.g:2265:4: (lv_first_0_0= ruleExpr )
            // InternalCalur.g:2266:5: lv_first_0_0= ruleExpr
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBinaryExprAccess().getFirstExprParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_first_0_0=ruleExpr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBinaryExprRule());
              					}
              					set(
              						current,
              						"first",
              						lv_first_0_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Expr");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCalur.g:2283:3: ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )?
            int alt28=2;
            alt28 = dfa28.predict(input);
            switch (alt28) {
                case 1 :
                    // InternalCalur.g:2284:4: ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) )
                    {
                    // InternalCalur.g:2284:4: ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) )
                    // InternalCalur.g:2285:5: ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator )
                    {
                    // InternalCalur.g:2289:5: (lv_operator_1_0= ruleBinaryOperator )
                    // InternalCalur.g:2290:6: lv_operator_1_0= ruleBinaryOperator
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getBinaryExprAccess().getOperatorBinaryOperatorParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_11);
                    lv_operator_1_0=ruleBinaryOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getBinaryExprRule());
                      						}
                      						set(
                      							current,
                      							"operator",
                      							lv_operator_1_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.BinaryOperator");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalCalur.g:2307:4: ( (lv_last_2_0= ruleBinaryExpr ) )
                    // InternalCalur.g:2308:5: (lv_last_2_0= ruleBinaryExpr )
                    {
                    // InternalCalur.g:2308:5: (lv_last_2_0= ruleBinaryExpr )
                    // InternalCalur.g:2309:6: lv_last_2_0= ruleBinaryExpr
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getBinaryExprAccess().getLastBinaryExprParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_last_2_0=ruleBinaryExpr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getBinaryExprRule());
                      						}
                      						set(
                      							current,
                      							"last",
                      							lv_last_2_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.BinaryExpr");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryExpr"


    // $ANTLR start "entryRuleUnaryOperator"
    // InternalCalur.g:2331:1: entryRuleUnaryOperator returns [String current=null] : iv_ruleUnaryOperator= ruleUnaryOperator EOF ;
    public final String entryRuleUnaryOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUnaryOperator = null;


        try {
            // InternalCalur.g:2331:53: (iv_ruleUnaryOperator= ruleUnaryOperator EOF )
            // InternalCalur.g:2332:2: iv_ruleUnaryOperator= ruleUnaryOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryOperator=ruleUnaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryOperator"


    // $ANTLR start "ruleUnaryOperator"
    // InternalCalur.g:2338:1: ruleUnaryOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_UnaryBooleanOperator_0= ruleUnaryBooleanOperator | this_UnaryArithmeticOperator_1= ruleUnaryArithmeticOperator ) ;
    public final AntlrDatatypeRuleToken ruleUnaryOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_UnaryBooleanOperator_0 = null;

        AntlrDatatypeRuleToken this_UnaryArithmeticOperator_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:2344:2: ( (this_UnaryBooleanOperator_0= ruleUnaryBooleanOperator | this_UnaryArithmeticOperator_1= ruleUnaryArithmeticOperator ) )
            // InternalCalur.g:2345:2: (this_UnaryBooleanOperator_0= ruleUnaryBooleanOperator | this_UnaryArithmeticOperator_1= ruleUnaryArithmeticOperator )
            {
            // InternalCalur.g:2345:2: (this_UnaryBooleanOperator_0= ruleUnaryBooleanOperator | this_UnaryArithmeticOperator_1= ruleUnaryArithmeticOperator )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==49) ) {
                alt29=1;
            }
            else if ( ((LA29_0>=56 && LA29_0<=59)) ) {
                alt29=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalCalur.g:2346:3: this_UnaryBooleanOperator_0= ruleUnaryBooleanOperator
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getUnaryOperatorAccess().getUnaryBooleanOperatorParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnaryBooleanOperator_0=ruleUnaryBooleanOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_UnaryBooleanOperator_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2357:3: this_UnaryArithmeticOperator_1= ruleUnaryArithmeticOperator
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getUnaryOperatorAccess().getUnaryArithmeticOperatorParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnaryArithmeticOperator_1=ruleUnaryArithmeticOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_UnaryArithmeticOperator_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "entryRuleZeroaryOperator"
    // InternalCalur.g:2371:1: entryRuleZeroaryOperator returns [String current=null] : iv_ruleZeroaryOperator= ruleZeroaryOperator EOF ;
    public final String entryRuleZeroaryOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleZeroaryOperator = null;


        try {
            // InternalCalur.g:2371:55: (iv_ruleZeroaryOperator= ruleZeroaryOperator EOF )
            // InternalCalur.g:2372:2: iv_ruleZeroaryOperator= ruleZeroaryOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getZeroaryOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleZeroaryOperator=ruleZeroaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleZeroaryOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZeroaryOperator"


    // $ANTLR start "ruleZeroaryOperator"
    // InternalCalur.g:2378:1: ruleZeroaryOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ZeroaryArithmeticOperator_0= ruleZeroaryArithmeticOperator | this_RTSFunctionSymbol_1= ruleRTSFunctionSymbol ) ;
    public final AntlrDatatypeRuleToken ruleZeroaryOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_ZeroaryArithmeticOperator_0 = null;

        AntlrDatatypeRuleToken this_RTSFunctionSymbol_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:2384:2: ( (this_ZeroaryArithmeticOperator_0= ruleZeroaryArithmeticOperator | this_RTSFunctionSymbol_1= ruleRTSFunctionSymbol ) )
            // InternalCalur.g:2385:2: (this_ZeroaryArithmeticOperator_0= ruleZeroaryArithmeticOperator | this_RTSFunctionSymbol_1= ruleRTSFunctionSymbol )
            {
            // InternalCalur.g:2385:2: (this_ZeroaryArithmeticOperator_0= ruleZeroaryArithmeticOperator | this_RTSFunctionSymbol_1= ruleRTSFunctionSymbol )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==60) ) {
                alt30=1;
            }
            else if ( ((LA30_0>=61 && LA30_0<=63)) ) {
                alt30=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalCalur.g:2386:3: this_ZeroaryArithmeticOperator_0= ruleZeroaryArithmeticOperator
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getZeroaryOperatorAccess().getZeroaryArithmeticOperatorParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ZeroaryArithmeticOperator_0=ruleZeroaryArithmeticOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ZeroaryArithmeticOperator_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2397:3: this_RTSFunctionSymbol_1= ruleRTSFunctionSymbol
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getZeroaryOperatorAccess().getRTSFunctionSymbolParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_RTSFunctionSymbol_1=ruleRTSFunctionSymbol();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_RTSFunctionSymbol_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZeroaryOperator"


    // $ANTLR start "entryRuleBinaryOperator"
    // InternalCalur.g:2411:1: entryRuleBinaryOperator returns [String current=null] : iv_ruleBinaryOperator= ruleBinaryOperator EOF ;
    public final String entryRuleBinaryOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBinaryOperator = null;


        try {
            // InternalCalur.g:2411:54: (iv_ruleBinaryOperator= ruleBinaryOperator EOF )
            // InternalCalur.g:2412:2: iv_ruleBinaryOperator= ruleBinaryOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryOperator=ruleBinaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryOperator"


    // $ANTLR start "ruleBinaryOperator"
    // InternalCalur.g:2418:1: ruleBinaryOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_BinaryBooleanOperator_0= ruleBinaryBooleanOperator | this_BinaryArithmeticOperator_1= ruleBinaryArithmeticOperator ) ;
    public final AntlrDatatypeRuleToken ruleBinaryOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_BinaryBooleanOperator_0 = null;

        AntlrDatatypeRuleToken this_BinaryArithmeticOperator_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:2424:2: ( (this_BinaryBooleanOperator_0= ruleBinaryBooleanOperator | this_BinaryArithmeticOperator_1= ruleBinaryArithmeticOperator ) )
            // InternalCalur.g:2425:2: (this_BinaryBooleanOperator_0= ruleBinaryBooleanOperator | this_BinaryArithmeticOperator_1= ruleBinaryArithmeticOperator )
            {
            // InternalCalur.g:2425:2: (this_BinaryBooleanOperator_0= ruleBinaryBooleanOperator | this_BinaryArithmeticOperator_1= ruleBinaryArithmeticOperator )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( ((LA31_0>=50 && LA31_0<=55)) ) {
                alt31=1;
            }
            else if ( ((LA31_0>=64 && LA31_0<=68)) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalCalur.g:2426:3: this_BinaryBooleanOperator_0= ruleBinaryBooleanOperator
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBinaryOperatorAccess().getBinaryBooleanOperatorParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BinaryBooleanOperator_0=ruleBinaryBooleanOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_BinaryBooleanOperator_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2437:3: this_BinaryArithmeticOperator_1= ruleBinaryArithmeticOperator
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBinaryOperatorAccess().getBinaryArithmeticOperatorParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BinaryArithmeticOperator_1=ruleBinaryArithmeticOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_BinaryArithmeticOperator_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryOperator"


    // $ANTLR start "entryRuleUnaryBooleanOperator"
    // InternalCalur.g:2451:1: entryRuleUnaryBooleanOperator returns [String current=null] : iv_ruleUnaryBooleanOperator= ruleUnaryBooleanOperator EOF ;
    public final String entryRuleUnaryBooleanOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUnaryBooleanOperator = null;


        try {
            // InternalCalur.g:2451:60: (iv_ruleUnaryBooleanOperator= ruleUnaryBooleanOperator EOF )
            // InternalCalur.g:2452:2: iv_ruleUnaryBooleanOperator= ruleUnaryBooleanOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryBooleanOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryBooleanOperator=ruleUnaryBooleanOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryBooleanOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryBooleanOperator"


    // $ANTLR start "ruleUnaryBooleanOperator"
    // InternalCalur.g:2458:1: ruleUnaryBooleanOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'not' ;
    public final AntlrDatatypeRuleToken ruleUnaryBooleanOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2464:2: (kw= 'not' )
            // InternalCalur.g:2465:2: kw= 'not'
            {
            kw=(Token)match(input,49,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(kw);
              		newLeafNode(kw, grammarAccess.getUnaryBooleanOperatorAccess().getNotKeyword());
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryBooleanOperator"


    // $ANTLR start "entryRuleBinaryBooleanOperator"
    // InternalCalur.g:2473:1: entryRuleBinaryBooleanOperator returns [String current=null] : iv_ruleBinaryBooleanOperator= ruleBinaryBooleanOperator EOF ;
    public final String entryRuleBinaryBooleanOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBinaryBooleanOperator = null;


        try {
            // InternalCalur.g:2473:61: (iv_ruleBinaryBooleanOperator= ruleBinaryBooleanOperator EOF )
            // InternalCalur.g:2474:2: iv_ruleBinaryBooleanOperator= ruleBinaryBooleanOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryBooleanOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryBooleanOperator=ruleBinaryBooleanOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryBooleanOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryBooleanOperator"


    // $ANTLR start "ruleBinaryBooleanOperator"
    // InternalCalur.g:2480:1: ruleBinaryBooleanOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '<' | kw= '>' | kw= '<=' | kw= '>=' | kw= '=' | kw= '<>' ) ;
    public final AntlrDatatypeRuleToken ruleBinaryBooleanOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2486:2: ( (kw= '<' | kw= '>' | kw= '<=' | kw= '>=' | kw= '=' | kw= '<>' ) )
            // InternalCalur.g:2487:2: (kw= '<' | kw= '>' | kw= '<=' | kw= '>=' | kw= '=' | kw= '<>' )
            {
            // InternalCalur.g:2487:2: (kw= '<' | kw= '>' | kw= '<=' | kw= '>=' | kw= '=' | kw= '<>' )
            int alt32=6;
            switch ( input.LA(1) ) {
            case 50:
                {
                alt32=1;
                }
                break;
            case 51:
                {
                alt32=2;
                }
                break;
            case 52:
                {
                alt32=3;
                }
                break;
            case 53:
                {
                alt32=4;
                }
                break;
            case 54:
                {
                alt32=5;
                }
                break;
            case 55:
                {
                alt32=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalCalur.g:2488:3: kw= '<'
                    {
                    kw=(Token)match(input,50,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getLessThanSignKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2494:3: kw= '>'
                    {
                    kw=(Token)match(input,51,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getGreaterThanSignKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:2500:3: kw= '<='
                    {
                    kw=(Token)match(input,52,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getLessThanSignEqualsSignKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:2506:3: kw= '>='
                    {
                    kw=(Token)match(input,53,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getGreaterThanSignEqualsSignKeyword_3());
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalCalur.g:2512:3: kw= '='
                    {
                    kw=(Token)match(input,54,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getEqualsSignKeyword_4());
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalCalur.g:2518:3: kw= '<>'
                    {
                    kw=(Token)match(input,55,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryBooleanOperatorAccess().getLessThanSignGreaterThanSignKeyword_5());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryBooleanOperator"


    // $ANTLR start "entryRuleUnaryArithmeticOperator"
    // InternalCalur.g:2527:1: entryRuleUnaryArithmeticOperator returns [String current=null] : iv_ruleUnaryArithmeticOperator= ruleUnaryArithmeticOperator EOF ;
    public final String entryRuleUnaryArithmeticOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUnaryArithmeticOperator = null;


        try {
            // InternalCalur.g:2527:63: (iv_ruleUnaryArithmeticOperator= ruleUnaryArithmeticOperator EOF )
            // InternalCalur.g:2528:2: iv_ruleUnaryArithmeticOperator= ruleUnaryArithmeticOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryArithmeticOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryArithmeticOperator=ruleUnaryArithmeticOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryArithmeticOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryArithmeticOperator"


    // $ANTLR start "ruleUnaryArithmeticOperator"
    // InternalCalur.g:2534:1: ruleUnaryArithmeticOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'sqrt' | kw= 'abs' | kw= 'ceil' | kw= 'floor' ) ;
    public final AntlrDatatypeRuleToken ruleUnaryArithmeticOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2540:2: ( (kw= 'sqrt' | kw= 'abs' | kw= 'ceil' | kw= 'floor' ) )
            // InternalCalur.g:2541:2: (kw= 'sqrt' | kw= 'abs' | kw= 'ceil' | kw= 'floor' )
            {
            // InternalCalur.g:2541:2: (kw= 'sqrt' | kw= 'abs' | kw= 'ceil' | kw= 'floor' )
            int alt33=4;
            switch ( input.LA(1) ) {
            case 56:
                {
                alt33=1;
                }
                break;
            case 57:
                {
                alt33=2;
                }
                break;
            case 58:
                {
                alt33=3;
                }
                break;
            case 59:
                {
                alt33=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }

            switch (alt33) {
                case 1 :
                    // InternalCalur.g:2542:3: kw= 'sqrt'
                    {
                    kw=(Token)match(input,56,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getUnaryArithmeticOperatorAccess().getSqrtKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2548:3: kw= 'abs'
                    {
                    kw=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getUnaryArithmeticOperatorAccess().getAbsKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:2554:3: kw= 'ceil'
                    {
                    kw=(Token)match(input,58,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getUnaryArithmeticOperatorAccess().getCeilKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:2560:3: kw= 'floor'
                    {
                    kw=(Token)match(input,59,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getUnaryArithmeticOperatorAccess().getFloorKeyword_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryArithmeticOperator"


    // $ANTLR start "entryRuleZeroaryArithmeticOperator"
    // InternalCalur.g:2569:1: entryRuleZeroaryArithmeticOperator returns [String current=null] : iv_ruleZeroaryArithmeticOperator= ruleZeroaryArithmeticOperator EOF ;
    public final String entryRuleZeroaryArithmeticOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleZeroaryArithmeticOperator = null;


        try {
            // InternalCalur.g:2569:65: (iv_ruleZeroaryArithmeticOperator= ruleZeroaryArithmeticOperator EOF )
            // InternalCalur.g:2570:2: iv_ruleZeroaryArithmeticOperator= ruleZeroaryArithmeticOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getZeroaryArithmeticOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleZeroaryArithmeticOperator=ruleZeroaryArithmeticOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleZeroaryArithmeticOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZeroaryArithmeticOperator"


    // $ANTLR start "ruleZeroaryArithmeticOperator"
    // InternalCalur.g:2576:1: ruleZeroaryArithmeticOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'rand' ;
    public final AntlrDatatypeRuleToken ruleZeroaryArithmeticOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2582:2: (kw= 'rand' )
            // InternalCalur.g:2583:2: kw= 'rand'
            {
            kw=(Token)match(input,60,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(kw);
              		newLeafNode(kw, grammarAccess.getZeroaryArithmeticOperatorAccess().getRandKeyword());
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZeroaryArithmeticOperator"


    // $ANTLR start "entryRuleRTSFunctionSymbol"
    // InternalCalur.g:2591:1: entryRuleRTSFunctionSymbol returns [String current=null] : iv_ruleRTSFunctionSymbol= ruleRTSFunctionSymbol EOF ;
    public final String entryRuleRTSFunctionSymbol() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRTSFunctionSymbol = null;


        try {
            // InternalCalur.g:2591:57: (iv_ruleRTSFunctionSymbol= ruleRTSFunctionSymbol EOF )
            // InternalCalur.g:2592:2: iv_ruleRTSFunctionSymbol= ruleRTSFunctionSymbol EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRTSFunctionSymbolRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRTSFunctionSymbol=ruleRTSFunctionSymbol();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRTSFunctionSymbol.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRTSFunctionSymbol"


    // $ANTLR start "ruleRTSFunctionSymbol"
    // InternalCalur.g:2598:1: ruleRTSFunctionSymbol returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'capsuleName' | kw= 'capsulePartName' | kw= 'index' ) ;
    public final AntlrDatatypeRuleToken ruleRTSFunctionSymbol() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2604:2: ( (kw= 'capsuleName' | kw= 'capsulePartName' | kw= 'index' ) )
            // InternalCalur.g:2605:2: (kw= 'capsuleName' | kw= 'capsulePartName' | kw= 'index' )
            {
            // InternalCalur.g:2605:2: (kw= 'capsuleName' | kw= 'capsulePartName' | kw= 'index' )
            int alt34=3;
            switch ( input.LA(1) ) {
            case 61:
                {
                alt34=1;
                }
                break;
            case 62:
                {
                alt34=2;
                }
                break;
            case 63:
                {
                alt34=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }

            switch (alt34) {
                case 1 :
                    // InternalCalur.g:2606:3: kw= 'capsuleName'
                    {
                    kw=(Token)match(input,61,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getRTSFunctionSymbolAccess().getCapsuleNameKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2612:3: kw= 'capsulePartName'
                    {
                    kw=(Token)match(input,62,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getRTSFunctionSymbolAccess().getCapsulePartNameKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:2618:3: kw= 'index'
                    {
                    kw=(Token)match(input,63,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getRTSFunctionSymbolAccess().getIndexKeyword_2());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRTSFunctionSymbol"


    // $ANTLR start "entryRuleBinaryArithmeticOperator"
    // InternalCalur.g:2627:1: entryRuleBinaryArithmeticOperator returns [String current=null] : iv_ruleBinaryArithmeticOperator= ruleBinaryArithmeticOperator EOF ;
    public final String entryRuleBinaryArithmeticOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBinaryArithmeticOperator = null;


        try {
            // InternalCalur.g:2627:64: (iv_ruleBinaryArithmeticOperator= ruleBinaryArithmeticOperator EOF )
            // InternalCalur.g:2628:2: iv_ruleBinaryArithmeticOperator= ruleBinaryArithmeticOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryArithmeticOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryArithmeticOperator=ruleBinaryArithmeticOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryArithmeticOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryArithmeticOperator"


    // $ANTLR start "ruleBinaryArithmeticOperator"
    // InternalCalur.g:2634:1: ruleBinaryArithmeticOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '+' | kw= '-' | kw= '*' | kw= '/' | kw= '%' ) ;
    public final AntlrDatatypeRuleToken ruleBinaryArithmeticOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:2640:2: ( (kw= '+' | kw= '-' | kw= '*' | kw= '/' | kw= '%' ) )
            // InternalCalur.g:2641:2: (kw= '+' | kw= '-' | kw= '*' | kw= '/' | kw= '%' )
            {
            // InternalCalur.g:2641:2: (kw= '+' | kw= '-' | kw= '*' | kw= '/' | kw= '%' )
            int alt35=5;
            switch ( input.LA(1) ) {
            case 64:
                {
                alt35=1;
                }
                break;
            case 65:
                {
                alt35=2;
                }
                break;
            case 66:
                {
                alt35=3;
                }
                break;
            case 67:
                {
                alt35=4;
                }
                break;
            case 68:
                {
                alt35=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }

            switch (alt35) {
                case 1 :
                    // InternalCalur.g:2642:3: kw= '+'
                    {
                    kw=(Token)match(input,64,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryArithmeticOperatorAccess().getPlusSignKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2648:3: kw= '-'
                    {
                    kw=(Token)match(input,65,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryArithmeticOperatorAccess().getHyphenMinusKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:2654:3: kw= '*'
                    {
                    kw=(Token)match(input,66,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryArithmeticOperatorAccess().getAsteriskKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:2660:3: kw= '/'
                    {
                    kw=(Token)match(input,67,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryArithmeticOperatorAccess().getSolidusKeyword_3());
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalCalur.g:2666:3: kw= '%'
                    {
                    kw=(Token)match(input,68,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBinaryArithmeticOperatorAccess().getPercentSignKeyword_4());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryArithmeticOperator"


    // $ANTLR start "entryRulePortRef"
    // InternalCalur.g:2675:1: entryRulePortRef returns [EObject current=null] : iv_rulePortRef= rulePortRef EOF ;
    public final EObject entryRulePortRef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePortRef = null;


        try {
            // InternalCalur.g:2675:48: (iv_rulePortRef= rulePortRef EOF )
            // InternalCalur.g:2676:2: iv_rulePortRef= rulePortRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPortRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePortRef=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePortRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePortRef"


    // $ANTLR start "rulePortRef"
    // InternalCalur.g:2682:1: rulePortRef returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? ) ;
    public final EObject rulePortRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_index_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2688:2: ( ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? ) )
            // InternalCalur.g:2689:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? )
            {
            // InternalCalur.g:2689:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? )
            // InternalCalur.g:2690:3: ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )?
            {
            // InternalCalur.g:2690:3: ( (otherlv_0= RULE_ID ) )
            // InternalCalur.g:2691:4: (otherlv_0= RULE_ID )
            {
            // InternalCalur.g:2691:4: (otherlv_0= RULE_ID )
            // InternalCalur.g:2692:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getPortRefRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getPortRefAccess().getPortPortCrossReference_0_0());
              				
            }

            }


            }

            // InternalCalur.g:2703:3: (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==21) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalCalur.g:2704:4: otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) )
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getPortRefAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalCalur.g:2708:4: ( (lv_index_2_0= ruleIndex ) )
                    // InternalCalur.g:2709:5: (lv_index_2_0= ruleIndex )
                    {
                    // InternalCalur.g:2709:5: (lv_index_2_0= ruleIndex )
                    // InternalCalur.g:2710:6: lv_index_2_0= ruleIndex
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getPortRefAccess().getIndexIndexParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_index_2_0=ruleIndex();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getPortRefRule());
                      						}
                      						set(
                      							current,
                      							"index",
                      							lv_index_2_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.Index");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePortRef"


    // $ANTLR start "entryRulePartRef"
    // InternalCalur.g:2732:1: entryRulePartRef returns [EObject current=null] : iv_rulePartRef= rulePartRef EOF ;
    public final EObject entryRulePartRef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePartRef = null;


        try {
            // InternalCalur.g:2732:48: (iv_rulePartRef= rulePartRef EOF )
            // InternalCalur.g:2733:2: iv_rulePartRef= rulePartRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPartRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePartRef=rulePartRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePartRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePartRef"


    // $ANTLR start "rulePartRef"
    // InternalCalur.g:2739:1: rulePartRef returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? ) ;
    public final EObject rulePartRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_index_2_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2745:2: ( ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? ) )
            // InternalCalur.g:2746:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? )
            {
            // InternalCalur.g:2746:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )? )
            // InternalCalur.g:2747:3: ( (otherlv_0= RULE_ID ) ) (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )?
            {
            // InternalCalur.g:2747:3: ( (otherlv_0= RULE_ID ) )
            // InternalCalur.g:2748:4: (otherlv_0= RULE_ID )
            {
            // InternalCalur.g:2748:4: (otherlv_0= RULE_ID )
            // InternalCalur.g:2749:5: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getPartRefRule());
              					}
              				
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_0, grammarAccess.getPartRefAccess().getPartPropertyCrossReference_0_0());
              				
            }

            }


            }

            // InternalCalur.g:2760:3: (otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==21) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalCalur.g:2761:4: otherlv_1= ':' ( (lv_index_2_0= ruleIndex ) )
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getPartRefAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalCalur.g:2765:4: ( (lv_index_2_0= ruleIndex ) )
                    // InternalCalur.g:2766:5: (lv_index_2_0= ruleIndex )
                    {
                    // InternalCalur.g:2766:5: (lv_index_2_0= ruleIndex )
                    // InternalCalur.g:2767:6: lv_index_2_0= ruleIndex
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getPartRefAccess().getIndexIndexParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_index_2_0=ruleIndex();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getPartRefRule());
                      						}
                      						set(
                      							current,
                      							"index",
                      							lv_index_2_0,
                      							"ca.queensu.cs.umlrt.calur.Calur.Index");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePartRef"


    // $ANTLR start "entryRuleIndex"
    // InternalCalur.g:2789:1: entryRuleIndex returns [EObject current=null] : iv_ruleIndex= ruleIndex EOF ;
    public final EObject entryRuleIndex() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIndex = null;


        try {
            // InternalCalur.g:2789:46: (iv_ruleIndex= ruleIndex EOF )
            // InternalCalur.g:2790:2: iv_ruleIndex= ruleIndex EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIndexRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIndex=ruleIndex();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIndex; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIndex"


    // $ANTLR start "ruleIndex"
    // InternalCalur.g:2796:1: ruleIndex returns [EObject current=null] : (this_Property_0= ruleProperty | this_LiteralInteger_1= ruleLiteralInteger ) ;
    public final EObject ruleIndex() throws RecognitionException {
        EObject current = null;

        EObject this_Property_0 = null;

        EObject this_LiteralInteger_1 = null;



        	enterRule();

        try {
            // InternalCalur.g:2802:2: ( (this_Property_0= ruleProperty | this_LiteralInteger_1= ruleLiteralInteger ) )
            // InternalCalur.g:2803:2: (this_Property_0= ruleProperty | this_LiteralInteger_1= ruleLiteralInteger )
            {
            // InternalCalur.g:2803:2: (this_Property_0= ruleProperty | this_LiteralInteger_1= ruleLiteralInteger )
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==RULE_ID) ) {
                alt38=1;
            }
            else if ( (LA38_0==RULE_INT) ) {
                alt38=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }
            switch (alt38) {
                case 1 :
                    // InternalCalur.g:2804:3: this_Property_0= ruleProperty
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIndexAccess().getPropertyParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Property_0=ruleProperty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Property_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2813:3: this_LiteralInteger_1= ruleLiteralInteger
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIndexAccess().getLiteralIntegerParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralInteger_1=ruleLiteralInteger();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralInteger_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIndex"


    // $ANTLR start "entryRuleValueSpecification"
    // InternalCalur.g:2825:1: entryRuleValueSpecification returns [EObject current=null] : iv_ruleValueSpecification= ruleValueSpecification EOF ;
    public final EObject entryRuleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueSpecification = null;


        try {
            // InternalCalur.g:2825:59: (iv_ruleValueSpecification= ruleValueSpecification EOF )
            // InternalCalur.g:2826:2: iv_ruleValueSpecification= ruleValueSpecification EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueSpecificationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleValueSpecification=ruleValueSpecification();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValueSpecification; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueSpecification"


    // $ANTLR start "ruleValueSpecification"
    // InternalCalur.g:2832:1: ruleValueSpecification returns [EObject current=null] : this_Literal_0= ruleLiteral ;
    public final EObject ruleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject this_Literal_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2838:2: (this_Literal_0= ruleLiteral )
            // InternalCalur.g:2839:2: this_Literal_0= ruleLiteral
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getValueSpecificationAccess().getLiteralParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_Literal_0=ruleLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_Literal_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueSpecification"


    // $ANTLR start "entryRuleLiteral"
    // InternalCalur.g:2850:1: entryRuleLiteral returns [EObject current=null] : iv_ruleLiteral= ruleLiteral EOF ;
    public final EObject entryRuleLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteral = null;


        try {
            // InternalCalur.g:2850:48: (iv_ruleLiteral= ruleLiteral EOF )
            // InternalCalur.g:2851:2: iv_ruleLiteral= ruleLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteral=ruleLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteral; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalCalur.g:2857:1: ruleLiteral returns [EObject current=null] : (this_LiteralNull_0= ruleLiteralNull | this_LiteralBoolean_1= ruleLiteralBoolean | this_LiteralInteger_2= ruleLiteralInteger | this_LiteralReal_3= ruleLiteralReal | this_LiteralString_4= ruleLiteralString ) ;
    public final EObject ruleLiteral() throws RecognitionException {
        EObject current = null;

        EObject this_LiteralNull_0 = null;

        EObject this_LiteralBoolean_1 = null;

        EObject this_LiteralInteger_2 = null;

        EObject this_LiteralReal_3 = null;

        EObject this_LiteralString_4 = null;



        	enterRule();

        try {
            // InternalCalur.g:2863:2: ( (this_LiteralNull_0= ruleLiteralNull | this_LiteralBoolean_1= ruleLiteralBoolean | this_LiteralInteger_2= ruleLiteralInteger | this_LiteralReal_3= ruleLiteralReal | this_LiteralString_4= ruleLiteralString ) )
            // InternalCalur.g:2864:2: (this_LiteralNull_0= ruleLiteralNull | this_LiteralBoolean_1= ruleLiteralBoolean | this_LiteralInteger_2= ruleLiteralInteger | this_LiteralReal_3= ruleLiteralReal | this_LiteralString_4= ruleLiteralString )
            {
            // InternalCalur.g:2864:2: (this_LiteralNull_0= ruleLiteralNull | this_LiteralBoolean_1= ruleLiteralBoolean | this_LiteralInteger_2= ruleLiteralInteger | this_LiteralReal_3= ruleLiteralReal | this_LiteralString_4= ruleLiteralString )
            int alt39=5;
            switch ( input.LA(1) ) {
            case 69:
                {
                alt39=1;
                }
                break;
            case 70:
            case 71:
                {
                alt39=2;
                }
                break;
            case RULE_INT:
                {
                alt39=3;
                }
                break;
            case RULE_FLOAT:
                {
                alt39=4;
                }
                break;
            case RULE_STRING:
                {
                alt39=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // InternalCalur.g:2865:3: this_LiteralNull_0= ruleLiteralNull
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getLiteralAccess().getLiteralNullParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralNull_0=ruleLiteralNull();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralNull_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:2874:3: this_LiteralBoolean_1= ruleLiteralBoolean
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getLiteralAccess().getLiteralBooleanParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralBoolean_1=ruleLiteralBoolean();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralBoolean_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCalur.g:2883:3: this_LiteralInteger_2= ruleLiteralInteger
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getLiteralAccess().getLiteralIntegerParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralInteger_2=ruleLiteralInteger();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralInteger_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCalur.g:2892:3: this_LiteralReal_3= ruleLiteralReal
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getLiteralAccess().getLiteralRealParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralReal_3=ruleLiteralReal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralReal_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalCalur.g:2901:3: this_LiteralString_4= ruleLiteralString
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getLiteralAccess().getLiteralStringParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_LiteralString_4=ruleLiteralString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_LiteralString_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleLiteralNull"
    // InternalCalur.g:2913:1: entryRuleLiteralNull returns [EObject current=null] : iv_ruleLiteralNull= ruleLiteralNull EOF ;
    public final EObject entryRuleLiteralNull() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralNull = null;


        try {
            // InternalCalur.g:2913:52: (iv_ruleLiteralNull= ruleLiteralNull EOF )
            // InternalCalur.g:2914:2: iv_ruleLiteralNull= ruleLiteralNull EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralNullRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteralNull=ruleLiteralNull();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralNull; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralNull"


    // $ANTLR start "ruleLiteralNull"
    // InternalCalur.g:2920:1: ruleLiteralNull returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleLiteralNull() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalCalur.g:2926:2: ( ( () otherlv_1= 'null' ) )
            // InternalCalur.g:2927:2: ( () otherlv_1= 'null' )
            {
            // InternalCalur.g:2927:2: ( () otherlv_1= 'null' )
            // InternalCalur.g:2928:3: () otherlv_1= 'null'
            {
            // InternalCalur.g:2928:3: ()
            // InternalCalur.g:2929:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLiteralNullAccess().getLiteralNullAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,69,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getLiteralNullAccess().getNullKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralNull"


    // $ANTLR start "entryRuleLiteralBoolean"
    // InternalCalur.g:2943:1: entryRuleLiteralBoolean returns [EObject current=null] : iv_ruleLiteralBoolean= ruleLiteralBoolean EOF ;
    public final EObject entryRuleLiteralBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralBoolean = null;


        try {
            // InternalCalur.g:2943:55: (iv_ruleLiteralBoolean= ruleLiteralBoolean EOF )
            // InternalCalur.g:2944:2: iv_ruleLiteralBoolean= ruleLiteralBoolean EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralBooleanRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteralBoolean=ruleLiteralBoolean();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralBoolean; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralBoolean"


    // $ANTLR start "ruleLiteralBoolean"
    // InternalCalur.g:2950:1: ruleLiteralBoolean returns [EObject current=null] : ( () ( (lv_value_1_0= ruleBool ) ) ) ;
    public final EObject ruleLiteralBoolean() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_1_0 = null;



        	enterRule();

        try {
            // InternalCalur.g:2956:2: ( ( () ( (lv_value_1_0= ruleBool ) ) ) )
            // InternalCalur.g:2957:2: ( () ( (lv_value_1_0= ruleBool ) ) )
            {
            // InternalCalur.g:2957:2: ( () ( (lv_value_1_0= ruleBool ) ) )
            // InternalCalur.g:2958:3: () ( (lv_value_1_0= ruleBool ) )
            {
            // InternalCalur.g:2958:3: ()
            // InternalCalur.g:2959:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLiteralBooleanAccess().getLiteralBooleanAction_0(),
              					current);
              			
            }

            }

            // InternalCalur.g:2965:3: ( (lv_value_1_0= ruleBool ) )
            // InternalCalur.g:2966:4: (lv_value_1_0= ruleBool )
            {
            // InternalCalur.g:2966:4: (lv_value_1_0= ruleBool )
            // InternalCalur.g:2967:5: lv_value_1_0= ruleBool
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getLiteralBooleanAccess().getValueBoolParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleBool();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getLiteralBooleanRule());
              					}
              					set(
              						current,
              						"value",
              						lv_value_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.Bool");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralBoolean"


    // $ANTLR start "entryRuleLiteralInteger"
    // InternalCalur.g:2988:1: entryRuleLiteralInteger returns [EObject current=null] : iv_ruleLiteralInteger= ruleLiteralInteger EOF ;
    public final EObject entryRuleLiteralInteger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralInteger = null;


        try {
            // InternalCalur.g:2988:55: (iv_ruleLiteralInteger= ruleLiteralInteger EOF )
            // InternalCalur.g:2989:2: iv_ruleLiteralInteger= ruleLiteralInteger EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralIntegerRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteralInteger=ruleLiteralInteger();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralInteger; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralInteger"


    // $ANTLR start "ruleLiteralInteger"
    // InternalCalur.g:2995:1: ruleLiteralInteger returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleLiteralInteger() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalCalur.g:3001:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalCalur.g:3002:2: ( () ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalCalur.g:3002:2: ( () ( (lv_value_1_0= RULE_INT ) ) )
            // InternalCalur.g:3003:3: () ( (lv_value_1_0= RULE_INT ) )
            {
            // InternalCalur.g:3003:3: ()
            // InternalCalur.g:3004:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLiteralIntegerAccess().getLiteralIntegerAction_0(),
              					current);
              			
            }

            }

            // InternalCalur.g:3010:3: ( (lv_value_1_0= RULE_INT ) )
            // InternalCalur.g:3011:4: (lv_value_1_0= RULE_INT )
            {
            // InternalCalur.g:3011:4: (lv_value_1_0= RULE_INT )
            // InternalCalur.g:3012:5: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getLiteralIntegerAccess().getValueINTTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLiteralIntegerRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.INT");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralInteger"


    // $ANTLR start "entryRuleLiteralReal"
    // InternalCalur.g:3032:1: entryRuleLiteralReal returns [EObject current=null] : iv_ruleLiteralReal= ruleLiteralReal EOF ;
    public final EObject entryRuleLiteralReal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralReal = null;


        try {
            // InternalCalur.g:3032:52: (iv_ruleLiteralReal= ruleLiteralReal EOF )
            // InternalCalur.g:3033:2: iv_ruleLiteralReal= ruleLiteralReal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralRealRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteralReal=ruleLiteralReal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralReal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralReal"


    // $ANTLR start "ruleLiteralReal"
    // InternalCalur.g:3039:1: ruleLiteralReal returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_FLOAT ) ) ) ;
    public final EObject ruleLiteralReal() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalCalur.g:3045:2: ( ( () ( (lv_value_1_0= RULE_FLOAT ) ) ) )
            // InternalCalur.g:3046:2: ( () ( (lv_value_1_0= RULE_FLOAT ) ) )
            {
            // InternalCalur.g:3046:2: ( () ( (lv_value_1_0= RULE_FLOAT ) ) )
            // InternalCalur.g:3047:3: () ( (lv_value_1_0= RULE_FLOAT ) )
            {
            // InternalCalur.g:3047:3: ()
            // InternalCalur.g:3048:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLiteralRealAccess().getLiteralRealAction_0(),
              					current);
              			
            }

            }

            // InternalCalur.g:3054:3: ( (lv_value_1_0= RULE_FLOAT ) )
            // InternalCalur.g:3055:4: (lv_value_1_0= RULE_FLOAT )
            {
            // InternalCalur.g:3055:4: (lv_value_1_0= RULE_FLOAT )
            // InternalCalur.g:3056:5: lv_value_1_0= RULE_FLOAT
            {
            lv_value_1_0=(Token)match(input,RULE_FLOAT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getLiteralRealAccess().getValueFLOATTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLiteralRealRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"ca.queensu.cs.umlrt.calur.Calur.FLOAT");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralReal"


    // $ANTLR start "entryRuleLiteralString"
    // InternalCalur.g:3076:1: entryRuleLiteralString returns [EObject current=null] : iv_ruleLiteralString= ruleLiteralString EOF ;
    public final EObject entryRuleLiteralString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralString = null;


        try {
            // InternalCalur.g:3076:54: (iv_ruleLiteralString= ruleLiteralString EOF )
            // InternalCalur.g:3077:2: iv_ruleLiteralString= ruleLiteralString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralStringRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteralString=ruleLiteralString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralString; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralString"


    // $ANTLR start "ruleLiteralString"
    // InternalCalur.g:3083:1: ruleLiteralString returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleLiteralString() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalCalur.g:3089:2: ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) )
            // InternalCalur.g:3090:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            {
            // InternalCalur.g:3090:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            // InternalCalur.g:3091:3: () ( (lv_value_1_0= RULE_STRING ) )
            {
            // InternalCalur.g:3091:3: ()
            // InternalCalur.g:3092:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLiteralStringAccess().getLiteralStringAction_0(),
              					current);
              			
            }

            }

            // InternalCalur.g:3098:3: ( (lv_value_1_0= RULE_STRING ) )
            // InternalCalur.g:3099:4: (lv_value_1_0= RULE_STRING )
            {
            // InternalCalur.g:3099:4: (lv_value_1_0= RULE_STRING )
            // InternalCalur.g:3100:5: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getLiteralStringAccess().getValueSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getLiteralStringRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralString"


    // $ANTLR start "entryRuleBool"
    // InternalCalur.g:3120:1: entryRuleBool returns [String current=null] : iv_ruleBool= ruleBool EOF ;
    public final String entryRuleBool() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBool = null;


        try {
            // InternalCalur.g:3120:44: (iv_ruleBool= ruleBool EOF )
            // InternalCalur.g:3121:2: iv_ruleBool= ruleBool EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBoolRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBool=ruleBool();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBool.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBool"


    // $ANTLR start "ruleBool"
    // InternalCalur.g:3127:1: ruleBool returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBool() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCalur.g:3133:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalCalur.g:3134:2: (kw= 'true' | kw= 'false' )
            {
            // InternalCalur.g:3134:2: (kw= 'true' | kw= 'false' )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==70) ) {
                alt40=1;
            }
            else if ( (LA40_0==71) ) {
                alt40=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }
            switch (alt40) {
                case 1 :
                    // InternalCalur.g:3135:3: kw= 'true'
                    {
                    kw=(Token)match(input,70,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBoolAccess().getTrueKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCalur.g:3141:3: kw= 'false'
                    {
                    kw=(Token)match(input,71,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBoolAccess().getFalseKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBool"


    // $ANTLR start "ruleUnit"
    // InternalCalur.g:3150:1: ruleUnit returns [Enumerator current=null] : ( (enumLiteral_0= 'seconds' ) | (enumLiteral_1= 'milliseconds' ) | (enumLiteral_2= 'microseconds' ) ) ;
    public final Enumerator ruleUnit() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalCalur.g:3156:2: ( ( (enumLiteral_0= 'seconds' ) | (enumLiteral_1= 'milliseconds' ) | (enumLiteral_2= 'microseconds' ) ) )
            // InternalCalur.g:3157:2: ( (enumLiteral_0= 'seconds' ) | (enumLiteral_1= 'milliseconds' ) | (enumLiteral_2= 'microseconds' ) )
            {
            // InternalCalur.g:3157:2: ( (enumLiteral_0= 'seconds' ) | (enumLiteral_1= 'milliseconds' ) | (enumLiteral_2= 'microseconds' ) )
            int alt41=3;
            switch ( input.LA(1) ) {
            case 72:
                {
                alt41=1;
                }
                break;
            case 73:
                {
                alt41=2;
                }
                break;
            case 74:
                {
                alt41=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }

            switch (alt41) {
                case 1 :
                    // InternalCalur.g:3158:3: (enumLiteral_0= 'seconds' )
                    {
                    // InternalCalur.g:3158:3: (enumLiteral_0= 'seconds' )
                    // InternalCalur.g:3159:4: enumLiteral_0= 'seconds'
                    {
                    enumLiteral_0=(Token)match(input,72,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getUnitAccess().getSECONDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getUnitAccess().getSECONDEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCalur.g:3166:3: (enumLiteral_1= 'milliseconds' )
                    {
                    // InternalCalur.g:3166:3: (enumLiteral_1= 'milliseconds' )
                    // InternalCalur.g:3167:4: enumLiteral_1= 'milliseconds'
                    {
                    enumLiteral_1=(Token)match(input,73,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getUnitAccess().getMILLISECONDSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getUnitAccess().getMILLISECONDSEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCalur.g:3174:3: (enumLiteral_2= 'microseconds' )
                    {
                    // InternalCalur.g:3174:3: (enumLiteral_2= 'microseconds' )
                    // InternalCalur.g:3175:4: enumLiteral_2= 'microseconds'
                    {
                    enumLiteral_2=(Token)match(input,74,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getUnitAccess().getMICROSECONDSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getUnitAccess().getMICROSECONDSEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnit"

    // $ANTLR start synpred1_InternalCalur
    public final void synpred1_InternalCalur_fragment() throws RecognitionException {   
        // InternalCalur.g:2285:5: ( ( ruleBinaryOperator ) )
        // InternalCalur.g:2285:6: ( ruleBinaryOperator )
        {
        // InternalCalur.g:2285:6: ( ruleBinaryOperator )
        // InternalCalur.g:2286:6: ruleBinaryOperator
        {
        pushFollow(FOLLOW_2);
        ruleBinaryOperator();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred1_InternalCalur

    // Delegated rules

    public final boolean synpred1_InternalCalur() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalCalur_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA28 dfa28 = new DFA28(this);
    static final String dfa_1s = "\27\uffff";
    static final String dfa_2s = "\1\5\2\uffff\1\25\17\uffff\1\5\1\26\1\15\1\uffff";
    static final String dfa_3s = "\1\53\2\uffff\1\52\17\uffff\1\5\1\36\1\52\1\uffff";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14\1\15\1\17\1\20\1\21\1\22\3\uffff\1\16";
    static final String dfa_5s = "\27\uffff}>";
    static final String[] dfa_6s = {
            "\1\3\6\uffff\1\20\1\uffff\1\1\5\uffff\1\2\1\uffff\1\7\4\uffff\1\10\1\4\1\5\1\6\1\11\1\12\1\13\2\uffff\1\14\1\16\1\21\1\22\1\uffff\1\15\1\uffff\1\17",
            "",
            "",
            "\1\24\14\uffff\1\15\5\uffff\1\15\1\uffff\1\23",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\25",
            "\1\7\5\uffff\1\4\1\uffff\1\6",
            "\1\26\24\uffff\1\15\5\uffff\1\15\1\uffff\1\15",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "115:2: (this_SendStatement_0= ruleSendStatement | this_LogStatement_1= ruleLogStatement | this_IncarnateStatement_2= ruleIncarnateStatement | this_DestroyStatement_3= ruleDestroyStatement | this_ImportStatement_4= ruleImportStatement | this_TimerStatement_5= ruleTimerStatement | this_CancelTimerStatement_6= ruleCancelTimerStatement | this_DeportStatement_7= ruleDeportStatement | this_RegisterStatement_8= ruleRegisterStatement | this_DeregisterStatement_9= ruleDeregisterStatement | this_VariableInitializationStatement_10= ruleVariableInitializationStatement | this_VariableAffectationStatement_11= ruleVariableAffectationStatement | this_CallStatement_12= ruleCallStatement | this_ObjectCallOperation_13= ruleObjectCallOperation | this_IfStatement_14= ruleIfStatement | this_VerbatimStatement_15= ruleVerbatimStatement | this_ReturnStatement_16= ruleReturnStatement | this_SrandStatement_17= ruleSrandStatement )";
        }
    }
    static final String dfa_7s = "\15\uffff";
    static final String dfa_8s = "\1\14\14\uffff";
    static final String dfa_9s = "\1\15\14\uffff";
    static final String dfa_10s = "\1\112\14\uffff";
    static final String dfa_11s = "\1\uffff\13\1\1\2";
    static final String dfa_12s = "\1\0\14\uffff}>";
    static final String[] dfa_13s = {
            "\1\14\12\uffff\1\14\12\uffff\1\14\10\uffff\1\14\5\uffff\1\1\1\2\1\3\1\4\1\5\1\6\10\uffff\1\7\1\10\1\11\1\12\1\13\3\uffff\3\14",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA28 extends DFA {

        public DFA28(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 28;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "2283:3: ( ( ( ( ruleBinaryOperator ) )=> (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_last_2_0= ruleBinaryExpr ) ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA28_0 = input.LA(1);

                         
                        int index28_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA28_0==50) && (synpred1_InternalCalur())) {s = 1;}

                        else if ( (LA28_0==51) && (synpred1_InternalCalur())) {s = 2;}

                        else if ( (LA28_0==52) && (synpred1_InternalCalur())) {s = 3;}

                        else if ( (LA28_0==53) && (synpred1_InternalCalur())) {s = 4;}

                        else if ( (LA28_0==54) && (synpred1_InternalCalur())) {s = 5;}

                        else if ( (LA28_0==55) && (synpred1_InternalCalur())) {s = 6;}

                        else if ( (LA28_0==64) && (synpred1_InternalCalur())) {s = 7;}

                        else if ( (LA28_0==65) && (synpred1_InternalCalur())) {s = 8;}

                        else if ( (LA28_0==66) && (synpred1_InternalCalur())) {s = 9;}

                        else if ( (LA28_0==67) && (synpred1_InternalCalur())) {s = 10;}

                        else if ( (LA28_0==68) && (synpred1_InternalCalur())) {s = 11;}

                        else if ( (LA28_0==EOF||LA28_0==13||LA28_0==24||LA28_0==35||LA28_0==44||(LA28_0>=72 && LA28_0<=74)) ) {s = 12;}

                         
                        input.seek(index28_0);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 28, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000AF3F8505022L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000042000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0xFF020204000800F0L,0x00000000000000E0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000003002000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000700L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000020000000020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0xFF02020C000800F0L,0x00000000000000E0L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x00004AF3F8505020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x00FC000000000002L,0x000000000000001FL});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000060L});

}