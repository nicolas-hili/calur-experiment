/**
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.calur;

import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.queensu.cs.umlrt.calur.calur.CalurPackage#getLocalVariable()
 * @model
 * @generated
 */
public interface LocalVariable extends TypedElement
{
} // LocalVariable
