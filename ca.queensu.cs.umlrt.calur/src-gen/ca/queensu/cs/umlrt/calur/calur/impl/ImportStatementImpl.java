/**
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.CalurPackage;
import ca.queensu.cs.umlrt.calur.calur.ImportStatement;
import ca.queensu.cs.umlrt.calur.calur.PartRef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.ImportStatementImpl#getCapsuleId <em>Capsule Id</em>}</li>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.ImportStatementImpl#getCapsuleName <em>Capsule Name</em>}</li>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.ImportStatementImpl#getPartRef <em>Part Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportStatementImpl extends StatementImpl implements ImportStatement
{
  /**
   * The cached value of the '{@link #getCapsuleId() <em>Capsule Id</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCapsuleId()
   * @generated
   * @ordered
   */
  protected Property capsuleId;

  /**
   * The cached value of the '{@link #getCapsuleName() <em>Capsule Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCapsuleName()
   * @generated
   * @ordered
   */
  protected org.eclipse.uml2.uml.Class capsuleName;

  /**
   * The cached value of the '{@link #getPartRef() <em>Part Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPartRef()
   * @generated
   * @ordered
   */
  protected PartRef partRef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImportStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CalurPackage.Literals.IMPORT_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property getCapsuleId()
  {
    if (capsuleId != null && capsuleId.eIsProxy())
    {
      InternalEObject oldCapsuleId = (InternalEObject)capsuleId;
      capsuleId = (Property)eResolveProxy(oldCapsuleId);
      if (capsuleId != oldCapsuleId)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CalurPackage.IMPORT_STATEMENT__CAPSULE_ID, oldCapsuleId, capsuleId));
      }
    }
    return capsuleId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property basicGetCapsuleId()
  {
    return capsuleId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCapsuleId(Property newCapsuleId)
  {
    Property oldCapsuleId = capsuleId;
    capsuleId = newCapsuleId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.IMPORT_STATEMENT__CAPSULE_ID, oldCapsuleId, capsuleId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.eclipse.uml2.uml.Class getCapsuleName()
  {
    if (capsuleName != null && capsuleName.eIsProxy())
    {
      InternalEObject oldCapsuleName = (InternalEObject)capsuleName;
      capsuleName = (org.eclipse.uml2.uml.Class)eResolveProxy(oldCapsuleName);
      if (capsuleName != oldCapsuleName)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME, oldCapsuleName, capsuleName));
      }
    }
    return capsuleName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.eclipse.uml2.uml.Class basicGetCapsuleName()
  {
    return capsuleName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCapsuleName(org.eclipse.uml2.uml.Class newCapsuleName)
  {
    org.eclipse.uml2.uml.Class oldCapsuleName = capsuleName;
    capsuleName = newCapsuleName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME, oldCapsuleName, capsuleName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PartRef getPartRef()
  {
    return partRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPartRef(PartRef newPartRef, NotificationChain msgs)
  {
    PartRef oldPartRef = partRef;
    partRef = newPartRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CalurPackage.IMPORT_STATEMENT__PART_REF, oldPartRef, newPartRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPartRef(PartRef newPartRef)
  {
    if (newPartRef != partRef)
    {
      NotificationChain msgs = null;
      if (partRef != null)
        msgs = ((InternalEObject)partRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CalurPackage.IMPORT_STATEMENT__PART_REF, null, msgs);
      if (newPartRef != null)
        msgs = ((InternalEObject)newPartRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CalurPackage.IMPORT_STATEMENT__PART_REF, null, msgs);
      msgs = basicSetPartRef(newPartRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.IMPORT_STATEMENT__PART_REF, newPartRef, newPartRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CalurPackage.IMPORT_STATEMENT__PART_REF:
        return basicSetPartRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_ID:
        if (resolve) return getCapsuleId();
        return basicGetCapsuleId();
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME:
        if (resolve) return getCapsuleName();
        return basicGetCapsuleName();
      case CalurPackage.IMPORT_STATEMENT__PART_REF:
        return getPartRef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_ID:
        setCapsuleId((Property)newValue);
        return;
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME:
        setCapsuleName((org.eclipse.uml2.uml.Class)newValue);
        return;
      case CalurPackage.IMPORT_STATEMENT__PART_REF:
        setPartRef((PartRef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_ID:
        setCapsuleId((Property)null);
        return;
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME:
        setCapsuleName((org.eclipse.uml2.uml.Class)null);
        return;
      case CalurPackage.IMPORT_STATEMENT__PART_REF:
        setPartRef((PartRef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_ID:
        return capsuleId != null;
      case CalurPackage.IMPORT_STATEMENT__CAPSULE_NAME:
        return capsuleName != null;
      case CalurPackage.IMPORT_STATEMENT__PART_REF:
        return partRef != null;
    }
    return super.eIsSet(featureID);
  }

} //ImportStatementImpl
