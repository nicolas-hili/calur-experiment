/**
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CalurFactoryImpl extends EFactoryImpl implements CalurFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CalurFactory init()
  {
    try
    {
      CalurFactory theCalurFactory = (CalurFactory)EPackage.Registry.INSTANCE.getEFactory(CalurPackage.eNS_URI);
      if (theCalurFactory != null)
      {
        return theCalurFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CalurFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CalurFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case CalurPackage.ACTION: return createAction();
      case CalurPackage.STATEMENT: return createStatement();
      case CalurPackage.VERBATIM_STATEMENT: return createVerbatimStatement();
      case CalurPackage.SEND_STATEMENT: return createSendStatement();
      case CalurPackage.LOG_STATEMENT: return createLogStatement();
      case CalurPackage.TIMER_STATEMENT: return createTimerStatement();
      case CalurPackage.INFORM_IN_TIMER_STATEMENT: return createInformInTimerStatement();
      case CalurPackage.INFORM_EVERY_TIMER_STATEMENT: return createInformEveryTimerStatement();
      case CalurPackage.CANCEL_TIMER_STATEMENT: return createCancelTimerStatement();
      case CalurPackage.TIME_SPEC: return createTimeSpec();
      case CalurPackage.INCARNATE_STATEMENT: return createIncarnateStatement();
      case CalurPackage.DESTROY_STATEMENT: return createDestroyStatement();
      case CalurPackage.IMPORT_STATEMENT: return createImportStatement();
      case CalurPackage.DEPORT_STATEMENT: return createDeportStatement();
      case CalurPackage.REGISTER_STATEMENT: return createRegisterStatement();
      case CalurPackage.DEREGISTER_STATEMENT: return createDeregisterStatement();
      case CalurPackage.BINARY_EXPR: return createBinaryExpr();
      case CalurPackage.VARIABLE_INITIALIZATION_STATEMENT: return createVariableInitializationStatement();
      case CalurPackage.CALL_STATEMENT: return createCallStatement();
      case CalurPackage.RETURN_STATEMENT: return createReturnStatement();
      case CalurPackage.SRAND_STATEMENT: return createSrandStatement();
      case CalurPackage.VARIABLE_AFFECTATION_STATEMENT: return createVariableAffectationStatement();
      case CalurPackage.VARIABLE_REF: return createVariableRef();
      case CalurPackage.GLOBAL_VARIABLE_REF: return createGlobalVariableRef();
      case CalurPackage.PROTOCOL_VARIABLE_REF: return createProtocolVariableRef();
      case CalurPackage.FIELD: return createField();
      case CalurPackage.PROPERTY: return createProperty();
      case CalurPackage.OPERATION: return createOperation();
      case CalurPackage.ARGUMENTS: return createArguments();
      case CalurPackage.OBJECT_CALL_OPERATION: return createObjectCallOperation();
      case CalurPackage.IF_STATEMENT: return createIfStatement();
      case CalurPackage.UNARY_EXPR: return createUnaryExpr();
      case CalurPackage.ZEROARY_EXPR: return createZeroaryExpr();
      case CalurPackage.PORT_REF: return createPortRef();
      case CalurPackage.PART_REF: return createPartRef();
      case CalurPackage.INDEX: return createIndex();
      case CalurPackage.VALUE_SPECIFICATION: return createValueSpecification();
      case CalurPackage.LITERAL_NULL: return createLiteralNull();
      case CalurPackage.LITERAL_BOOLEAN: return createLiteralBoolean();
      case CalurPackage.LITERAL_INTEGER: return createLiteralInteger();
      case CalurPackage.LITERAL_REAL: return createLiteralReal();
      case CalurPackage.LITERAL_STRING: return createLiteralString();
      case CalurPackage.LOCAL_VARIABLE: return createLocalVariable();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case CalurPackage.UNIT:
        return createUnitFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case CalurPackage.UNIT:
        return convertUnitToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VerbatimStatement createVerbatimStatement()
  {
    VerbatimStatementImpl verbatimStatement = new VerbatimStatementImpl();
    return verbatimStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendStatement createSendStatement()
  {
    SendStatementImpl sendStatement = new SendStatementImpl();
    return sendStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogStatement createLogStatement()
  {
    LogStatementImpl logStatement = new LogStatementImpl();
    return logStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerStatement createTimerStatement()
  {
    TimerStatementImpl timerStatement = new TimerStatementImpl();
    return timerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InformInTimerStatement createInformInTimerStatement()
  {
    InformInTimerStatementImpl informInTimerStatement = new InformInTimerStatementImpl();
    return informInTimerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InformEveryTimerStatement createInformEveryTimerStatement()
  {
    InformEveryTimerStatementImpl informEveryTimerStatement = new InformEveryTimerStatementImpl();
    return informEveryTimerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CancelTimerStatement createCancelTimerStatement()
  {
    CancelTimerStatementImpl cancelTimerStatement = new CancelTimerStatementImpl();
    return cancelTimerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeSpec createTimeSpec()
  {
    TimeSpecImpl timeSpec = new TimeSpecImpl();
    return timeSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IncarnateStatement createIncarnateStatement()
  {
    IncarnateStatementImpl incarnateStatement = new IncarnateStatementImpl();
    return incarnateStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DestroyStatement createDestroyStatement()
  {
    DestroyStatementImpl destroyStatement = new DestroyStatementImpl();
    return destroyStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportStatement createImportStatement()
  {
    ImportStatementImpl importStatement = new ImportStatementImpl();
    return importStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeportStatement createDeportStatement()
  {
    DeportStatementImpl deportStatement = new DeportStatementImpl();
    return deportStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RegisterStatement createRegisterStatement()
  {
    RegisterStatementImpl registerStatement = new RegisterStatementImpl();
    return registerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeregisterStatement createDeregisterStatement()
  {
    DeregisterStatementImpl deregisterStatement = new DeregisterStatementImpl();
    return deregisterStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BinaryExpr createBinaryExpr()
  {
    BinaryExprImpl binaryExpr = new BinaryExprImpl();
    return binaryExpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableInitializationStatement createVariableInitializationStatement()
  {
    VariableInitializationStatementImpl variableInitializationStatement = new VariableInitializationStatementImpl();
    return variableInitializationStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallStatement createCallStatement()
  {
    CallStatementImpl callStatement = new CallStatementImpl();
    return callStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnStatement createReturnStatement()
  {
    ReturnStatementImpl returnStatement = new ReturnStatementImpl();
    return returnStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SrandStatement createSrandStatement()
  {
    SrandStatementImpl srandStatement = new SrandStatementImpl();
    return srandStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableAffectationStatement createVariableAffectationStatement()
  {
    VariableAffectationStatementImpl variableAffectationStatement = new VariableAffectationStatementImpl();
    return variableAffectationStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableRef createVariableRef()
  {
    VariableRefImpl variableRef = new VariableRefImpl();
    return variableRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GlobalVariableRef createGlobalVariableRef()
  {
    GlobalVariableRefImpl globalVariableRef = new GlobalVariableRefImpl();
    return globalVariableRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProtocolVariableRef createProtocolVariableRef()
  {
    ProtocolVariableRefImpl protocolVariableRef = new ProtocolVariableRefImpl();
    return protocolVariableRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Field createField()
  {
    FieldImpl field = new FieldImpl();
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property createProperty()
  {
    PropertyImpl property = new PropertyImpl();
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Operation createOperation()
  {
    OperationImpl operation = new OperationImpl();
    return operation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Arguments createArguments()
  {
    ArgumentsImpl arguments = new ArgumentsImpl();
    return arguments;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObjectCallOperation createObjectCallOperation()
  {
    ObjectCallOperationImpl objectCallOperation = new ObjectCallOperationImpl();
    return objectCallOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IfStatement createIfStatement()
  {
    IfStatementImpl ifStatement = new IfStatementImpl();
    return ifStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryExpr createUnaryExpr()
  {
    UnaryExprImpl unaryExpr = new UnaryExprImpl();
    return unaryExpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ZeroaryExpr createZeroaryExpr()
  {
    ZeroaryExprImpl zeroaryExpr = new ZeroaryExprImpl();
    return zeroaryExpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRef createPortRef()
  {
    PortRefImpl portRef = new PortRefImpl();
    return portRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PartRef createPartRef()
  {
    PartRefImpl partRef = new PartRefImpl();
    return partRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Index createIndex()
  {
    IndexImpl index = new IndexImpl();
    return index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueSpecification createValueSpecification()
  {
    ValueSpecificationImpl valueSpecification = new ValueSpecificationImpl();
    return valueSpecification;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralNull createLiteralNull()
  {
    LiteralNullImpl literalNull = new LiteralNullImpl();
    return literalNull;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralBoolean createLiteralBoolean()
  {
    LiteralBooleanImpl literalBoolean = new LiteralBooleanImpl();
    return literalBoolean;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralInteger createLiteralInteger()
  {
    LiteralIntegerImpl literalInteger = new LiteralIntegerImpl();
    return literalInteger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralReal createLiteralReal()
  {
    LiteralRealImpl literalReal = new LiteralRealImpl();
    return literalReal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralString createLiteralString()
  {
    LiteralStringImpl literalString = new LiteralStringImpl();
    return literalString;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LocalVariable createLocalVariable()
  {
    LocalVariableImpl localVariable = new LocalVariableImpl();
    return localVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Unit createUnitFromString(EDataType eDataType, String initialValue)
  {
    Unit result = Unit.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertUnitToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CalurPackage getCalurPackage()
  {
    return (CalurPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CalurPackage getPackage()
  {
    return CalurPackage.eINSTANCE;
  }

} //CalurFactoryImpl
