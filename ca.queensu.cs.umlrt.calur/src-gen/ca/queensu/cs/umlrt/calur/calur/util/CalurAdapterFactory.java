/**
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.calur.util;

import ca.queensu.cs.umlrt.calur.calur.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.queensu.cs.umlrt.calur.calur.CalurPackage
 * @generated
 */
public class CalurAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CalurPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CalurAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = CalurPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CalurSwitch<Adapter> modelSwitch =
    new CalurSwitch<Adapter>()
    {
      @Override
      public Adapter caseAction(Action object)
      {
        return createActionAdapter();
      }
      @Override
      public Adapter caseStatement(Statement object)
      {
        return createStatementAdapter();
      }
      @Override
      public Adapter caseVerbatimStatement(VerbatimStatement object)
      {
        return createVerbatimStatementAdapter();
      }
      @Override
      public Adapter caseSendStatement(SendStatement object)
      {
        return createSendStatementAdapter();
      }
      @Override
      public Adapter caseLogStatement(LogStatement object)
      {
        return createLogStatementAdapter();
      }
      @Override
      public Adapter caseTimerStatement(TimerStatement object)
      {
        return createTimerStatementAdapter();
      }
      @Override
      public Adapter caseInformInTimerStatement(InformInTimerStatement object)
      {
        return createInformInTimerStatementAdapter();
      }
      @Override
      public Adapter caseInformEveryTimerStatement(InformEveryTimerStatement object)
      {
        return createInformEveryTimerStatementAdapter();
      }
      @Override
      public Adapter caseCancelTimerStatement(CancelTimerStatement object)
      {
        return createCancelTimerStatementAdapter();
      }
      @Override
      public Adapter caseTimeSpec(TimeSpec object)
      {
        return createTimeSpecAdapter();
      }
      @Override
      public Adapter caseIncarnateStatement(IncarnateStatement object)
      {
        return createIncarnateStatementAdapter();
      }
      @Override
      public Adapter caseDestroyStatement(DestroyStatement object)
      {
        return createDestroyStatementAdapter();
      }
      @Override
      public Adapter caseImportStatement(ImportStatement object)
      {
        return createImportStatementAdapter();
      }
      @Override
      public Adapter caseDeportStatement(DeportStatement object)
      {
        return createDeportStatementAdapter();
      }
      @Override
      public Adapter caseRegisterStatement(RegisterStatement object)
      {
        return createRegisterStatementAdapter();
      }
      @Override
      public Adapter caseDeregisterStatement(DeregisterStatement object)
      {
        return createDeregisterStatementAdapter();
      }
      @Override
      public Adapter caseBinaryExpr(BinaryExpr object)
      {
        return createBinaryExprAdapter();
      }
      @Override
      public Adapter caseVariableInitializationStatement(VariableInitializationStatement object)
      {
        return createVariableInitializationStatementAdapter();
      }
      @Override
      public Adapter caseCallStatement(CallStatement object)
      {
        return createCallStatementAdapter();
      }
      @Override
      public Adapter caseReturnStatement(ReturnStatement object)
      {
        return createReturnStatementAdapter();
      }
      @Override
      public Adapter caseSrandStatement(SrandStatement object)
      {
        return createSrandStatementAdapter();
      }
      @Override
      public Adapter caseVariableAffectationStatement(VariableAffectationStatement object)
      {
        return createVariableAffectationStatementAdapter();
      }
      @Override
      public Adapter caseVariableRef(VariableRef object)
      {
        return createVariableRefAdapter();
      }
      @Override
      public Adapter caseGlobalVariableRef(GlobalVariableRef object)
      {
        return createGlobalVariableRefAdapter();
      }
      @Override
      public Adapter caseProtocolVariableRef(ProtocolVariableRef object)
      {
        return createProtocolVariableRefAdapter();
      }
      @Override
      public Adapter caseField(Field object)
      {
        return createFieldAdapter();
      }
      @Override
      public Adapter caseProperty(Property object)
      {
        return createPropertyAdapter();
      }
      @Override
      public Adapter caseOperation(Operation object)
      {
        return createOperationAdapter();
      }
      @Override
      public Adapter caseArguments(Arguments object)
      {
        return createArgumentsAdapter();
      }
      @Override
      public Adapter caseObjectCallOperation(ObjectCallOperation object)
      {
        return createObjectCallOperationAdapter();
      }
      @Override
      public Adapter caseIfStatement(IfStatement object)
      {
        return createIfStatementAdapter();
      }
      @Override
      public Adapter caseUnaryExpr(UnaryExpr object)
      {
        return createUnaryExprAdapter();
      }
      @Override
      public Adapter caseZeroaryExpr(ZeroaryExpr object)
      {
        return createZeroaryExprAdapter();
      }
      @Override
      public Adapter casePortRef(PortRef object)
      {
        return createPortRefAdapter();
      }
      @Override
      public Adapter casePartRef(PartRef object)
      {
        return createPartRefAdapter();
      }
      @Override
      public Adapter caseIndex(Index object)
      {
        return createIndexAdapter();
      }
      @Override
      public Adapter caseValueSpecification(ValueSpecification object)
      {
        return createValueSpecificationAdapter();
      }
      @Override
      public Adapter caseLiteralNull(LiteralNull object)
      {
        return createLiteralNullAdapter();
      }
      @Override
      public Adapter caseLiteralBoolean(LiteralBoolean object)
      {
        return createLiteralBooleanAdapter();
      }
      @Override
      public Adapter caseLiteralInteger(LiteralInteger object)
      {
        return createLiteralIntegerAdapter();
      }
      @Override
      public Adapter caseLiteralReal(LiteralReal object)
      {
        return createLiteralRealAdapter();
      }
      @Override
      public Adapter caseLiteralString(LiteralString object)
      {
        return createLiteralStringAdapter();
      }
      @Override
      public Adapter caseLocalVariable(LocalVariable object)
      {
        return createLocalVariableAdapter();
      }
      @Override
      public Adapter caseEModelElement(EModelElement object)
      {
        return createEModelElementAdapter();
      }
      @Override
      public Adapter caseElement(Element object)
      {
        return createElementAdapter();
      }
      @Override
      public Adapter caseNamedElement(NamedElement object)
      {
        return createNamedElementAdapter();
      }
      @Override
      public Adapter caseTypedElement(TypedElement object)
      {
        return createTypedElementAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Action
   * @generated
   */
  public Adapter createActionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Statement
   * @generated
   */
  public Adapter createStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.VerbatimStatement <em>Verbatim Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.VerbatimStatement
   * @generated
   */
  public Adapter createVerbatimStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.SendStatement <em>Send Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.SendStatement
   * @generated
   */
  public Adapter createSendStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LogStatement <em>Log Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LogStatement
   * @generated
   */
  public Adapter createLogStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.TimerStatement <em>Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.TimerStatement
   * @generated
   */
  public Adapter createTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.InformInTimerStatement <em>Inform In Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.InformInTimerStatement
   * @generated
   */
  public Adapter createInformInTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.InformEveryTimerStatement <em>Inform Every Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.InformEveryTimerStatement
   * @generated
   */
  public Adapter createInformEveryTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.CancelTimerStatement <em>Cancel Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.CancelTimerStatement
   * @generated
   */
  public Adapter createCancelTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.TimeSpec <em>Time Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.TimeSpec
   * @generated
   */
  public Adapter createTimeSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.IncarnateStatement <em>Incarnate Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.IncarnateStatement
   * @generated
   */
  public Adapter createIncarnateStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.DestroyStatement <em>Destroy Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.DestroyStatement
   * @generated
   */
  public Adapter createDestroyStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ImportStatement <em>Import Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ImportStatement
   * @generated
   */
  public Adapter createImportStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.DeportStatement <em>Deport Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.DeportStatement
   * @generated
   */
  public Adapter createDeportStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.RegisterStatement <em>Register Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.RegisterStatement
   * @generated
   */
  public Adapter createRegisterStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.DeregisterStatement <em>Deregister Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.DeregisterStatement
   * @generated
   */
  public Adapter createDeregisterStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.BinaryExpr <em>Binary Expr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.BinaryExpr
   * @generated
   */
  public Adapter createBinaryExprAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.VariableInitializationStatement <em>Variable Initialization Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.VariableInitializationStatement
   * @generated
   */
  public Adapter createVariableInitializationStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.CallStatement <em>Call Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.CallStatement
   * @generated
   */
  public Adapter createCallStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ReturnStatement <em>Return Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ReturnStatement
   * @generated
   */
  public Adapter createReturnStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.SrandStatement <em>Srand Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.SrandStatement
   * @generated
   */
  public Adapter createSrandStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.VariableAffectationStatement <em>Variable Affectation Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.VariableAffectationStatement
   * @generated
   */
  public Adapter createVariableAffectationStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.VariableRef <em>Variable Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.VariableRef
   * @generated
   */
  public Adapter createVariableRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.GlobalVariableRef <em>Global Variable Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.GlobalVariableRef
   * @generated
   */
  public Adapter createGlobalVariableRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ProtocolVariableRef <em>Protocol Variable Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ProtocolVariableRef
   * @generated
   */
  public Adapter createProtocolVariableRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Field <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Field
   * @generated
   */
  public Adapter createFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Property
   * @generated
   */
  public Adapter createPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Operation <em>Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Operation
   * @generated
   */
  public Adapter createOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Arguments <em>Arguments</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Arguments
   * @generated
   */
  public Adapter createArgumentsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ObjectCallOperation <em>Object Call Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ObjectCallOperation
   * @generated
   */
  public Adapter createObjectCallOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.IfStatement <em>If Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.IfStatement
   * @generated
   */
  public Adapter createIfStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.UnaryExpr <em>Unary Expr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.UnaryExpr
   * @generated
   */
  public Adapter createUnaryExprAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ZeroaryExpr <em>Zeroary Expr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ZeroaryExpr
   * @generated
   */
  public Adapter createZeroaryExprAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.PortRef <em>Port Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.PortRef
   * @generated
   */
  public Adapter createPortRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.PartRef <em>Part Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.PartRef
   * @generated
   */
  public Adapter createPartRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.Index <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.Index
   * @generated
   */
  public Adapter createIndexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.ValueSpecification <em>Value Specification</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.ValueSpecification
   * @generated
   */
  public Adapter createValueSpecificationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LiteralNull <em>Literal Null</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LiteralNull
   * @generated
   */
  public Adapter createLiteralNullAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LiteralBoolean <em>Literal Boolean</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LiteralBoolean
   * @generated
   */
  public Adapter createLiteralBooleanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LiteralInteger <em>Literal Integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LiteralInteger
   * @generated
   */
  public Adapter createLiteralIntegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LiteralReal <em>Literal Real</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LiteralReal
   * @generated
   */
  public Adapter createLiteralRealAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LiteralString <em>Literal String</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LiteralString
   * @generated
   */
  public Adapter createLiteralStringAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ca.queensu.cs.umlrt.calur.calur.LocalVariable <em>Local Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ca.queensu.cs.umlrt.calur.calur.LocalVariable
   * @generated
   */
  public Adapter createLocalVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.eclipse.emf.ecore.EModelElement <em>EModel Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.eclipse.emf.ecore.EModelElement
   * @generated
   */
  public Adapter createEModelElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.eclipse.uml2.uml.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.eclipse.uml2.uml.Element
   * @generated
   */
  public Adapter createElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.eclipse.uml2.uml.NamedElement <em>Named Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.eclipse.uml2.uml.NamedElement
   * @generated
   */
  public Adapter createNamedElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.eclipse.uml2.uml.TypedElement <em>Typed Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.eclipse.uml2.uml.TypedElement
   * @generated
   */
  public Adapter createTypedElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //CalurAdapterFactory
