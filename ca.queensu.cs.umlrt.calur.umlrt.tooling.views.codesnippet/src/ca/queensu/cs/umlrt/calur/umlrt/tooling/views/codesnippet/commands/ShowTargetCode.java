package ca.queensu.cs.umlrt.calur.umlrt.tooling.views.codesnippet.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.BackingStoreException;

import ca.queensu.cs.umlrt.calur.umlrt.tooling.views.codesnippet.internal.Activator;
import ca.queensu.cs.umlrt.calur.umlrt.tooling.views.codesnippet.internal.CodeSnippetPage;
import ca.queensu.cs.umlrt.calur.umlrt.tooling.views.codesnippet.internal.CodeSnippetView;

public class ShowTargetCode extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		// Change the selection state and store it
		Command command = event.getCommand();
	    boolean oldValue = HandlerUtil.toggleCommandState(command);
	    storeSelection(!oldValue);
	    
	    // Refresh the view
	    CodeSnippetView part = (CodeSnippetView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage()
	    	    .findView(Activator.PLUGIN_ID);
	    ISelectionService selectionService = part.getSite().getWorkbenchWindow().getSelectionService();
	    ISelection selection = selectionService.getSelection();
	    ((CodeSnippetView)part).forceCommit();
	    part.selectionChanged(HandlerUtil.getActiveEditor(event), selection);
	    
		return null;
	}
	
	private void storeSelection(boolean selection) {
		IEclipsePreferences preferences = InstanceScope.INSTANCE
			    .getNode(Activator.PLUGIN_ID);
		preferences.put(Activator.SHOW_TARGET_CODE_PREFERENCE, Boolean.toString(selection));
	    try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		
		
	}

}
