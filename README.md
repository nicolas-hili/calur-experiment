# Core Action Language for UML-RT #

Core Action Language for UML-RT (CALUR) is a small action language dedicated for UML-RT. This repository contains the definition of Calur and an extension of the Code Snippet View in Papyrus-RT for supporting Calur.

### How to install? ###

The easiest way to install Calur in Papyrus-RT is using the following [update site for Papyrus-RT user version 1.0 on Oxygen](http://mase.cs.queensu.ca/modeling/calur/updates/nightly/oxygen/)

#### Steps to install ####

* If not already done, install [Papyrus-RT user version 1.0](https://www.eclipse.org/papyrus-rt/content/download.php);
* Once Papyrus-RT is opened, go to *Help/Install New Software...*
* Add the following update site: `http://mase.cs.queensu.ca/modeling/calur/updates/nightly/oxygen/` ;
* If necessary, uncheck *Group items by category* ;
* Select *Papyrus-RT Support for Calur* and hit the *Finish* button ;
* Restart Eclipse.

### First use ###

Calur is still experimental and therefore does not extend the *Code Snippet View* of Papyrus-RT. Instead, it duplicated the *Code Snippet View*. To use Calur for the first time:

* Make sure the existing *Code Snippet View* has been closed ;
* Go to *Window/Show View/Other...*
* Under the *Papyrus-RT categoery*, select *Calur Code Snippet*.

The new view adds a button on the top right corner. This button can be used for easily switch between the target language (C++) and Calur.

### Coverage ###

The following describes the current coverage of Calur by our implementation


| Statements        | Support           | Comments  |
| ----------------- |:-----------------:| ----------|
| Send              | Supported         |  |
| Incarnate         | Supported         | |
| Destroy           | Supported         |            |
| Import            | Supported         |  |
| Deport            | Supported         | |
| Register          | Not yet supported |           |
| Deregister        | Not yet supported |           |
| Log               | Supported         |           |
| Inform in         | Supported         |           |
| Inform every      | Supported         |           |
| Cancel timer      | Supported         |           |
| Var (variable definition)  | Supported | Support for UMLPrimiviteTypes and PassiveClassProperties |
| (variable affectation)     | Partially Supported | No support yet for complex type nor operation return values|


### Contributors ###

- Queen's University
- - Nicolas Hili <hili@cs.queensu.ca>
- - Juergen Dingel <dingel@cs.queensu.ca>
- Zeligsoft
- - Ernesto Posse <eposse@zeligsoft.com>