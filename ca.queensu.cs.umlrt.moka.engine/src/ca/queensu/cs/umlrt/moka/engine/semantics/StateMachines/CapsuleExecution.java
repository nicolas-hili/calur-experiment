package ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.fuml.Semantics.Classes.Kernel.IObject_;
import org.eclipse.papyrus.moka.fuml.Semantics.Classes.Kernel.IValue;
import org.eclipse.papyrus.moka.fuml.Semantics.impl.CommonBehaviors.BasicBehaviors.Execution;
import org.eclipse.papyrus.moka.fuml.statemachines.Semantics.StateMachines.RegionActivation;
import org.eclipse.papyrus.moka.fuml.statemachines.interfaces.Semantics.StateMachines.IRegionActivation;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;

public class CapsuleExecution  extends Execution {


	/**
	 * List of visitors associated to regions owned by the state-machine
	 */
	protected List<IRegionActivation> regionActivation;
	
	public CapsuleExecution(){
		super();
		this.regionActivation = new ArrayList<IRegionActivation>();
	}
	
	public CapsuleExecution(IObject_ context){
		super();
		this.regionActivation = new ArrayList<IRegionActivation>();
		this.context = context;
	}
	
	
	@Override
	public void execute() {
		UMLRTStateMachineExecution stateMachineExecution = new UMLRTStateMachineExecution();
		stateMachineExecution.addType((Class) this.getTypes().get(0));
		stateMachineExecution.setLocus(getLocus());
		stateMachineExecution.startBehavior((Class) this.getTypes().get(0).getOwner(), null);
/*		this.initRegions();
		for(IRegionActivation activation: this.regionActivation){
			activation.activate();
		}
		for(IRegionActivation activation: this.regionActivation){
			activation.activateTransitions();
		}
		for(IRegionActivation regionActivation: this.regionActivation){
			regionActivation.enter(null, null);
		} */
	}
	
	@Override
	public IValue new_() {
		if(this.context!=null){
			return new CapsuleExecution(this.context);
		}
		return new CapsuleExecution();
	}
	
	protected void initRegions(){
		StateMachine machine = null;
		if(!this.getTypes().isEmpty()){
		//	machine = (StateMachine) ((org.eclipse.uml2.uml.Class) this.getTypes().get(0)).getOwnedBehaviors().get(0);
			machine =  (StateMachine) this.getTypes().get(0);
		}
		if(machine!=null){
			for(Region region: machine.getRegions()){
				RegionActivation activation = (RegionActivation) this.locus.getFactory().instantiateVisitor(region);
				activation.setParent(this);
				activation.setNode(region);
				this.regionActivation.add(activation);
			}
		}
	}

}