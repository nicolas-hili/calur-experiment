package ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines;

import org.eclipse.papyrus.moka.fuml.Semantics.CommonBehaviors.Communications.IEventOccurrence;
import org.eclipse.papyrus.moka.fuml.Semantics.impl.CommonBehaviors.Communications.GetNextEventStrategy;
import org.eclipse.papyrus.moka.fuml.Semantics.impl.CommonBehaviors.Communications.ObjectActivation;

public class NextEventStrategy extends GetNextEventStrategy {

	@Override
	public IEventOccurrence getNextEvent(ObjectActivation objectActivation) {
		// TODO Auto-generated method stub
		System.out.println(objectActivation.getEvents());
		return objectActivation.getEvents().get(0);
	}
	
	

}
