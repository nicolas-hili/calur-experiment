package ca.queensu.cs.umlrt.moka.engine.semantics.Loci;

import org.eclipse.papyrus.moka.fuml.Semantics.CommonBehaviors.BasicBehaviors.IOpaqueBehaviorExecution;
import org.eclipse.papyrus.moka.fuml.Semantics.Loci.LociL1.ISemanticVisitor;
import org.eclipse.papyrus.moka.fuml.statemachines.Semantics.Loci.SM_ExecutionFactory;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.StateMachine;

import ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines.CalurBehaviorExecution;
import ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines.CapsuleExecution;
import ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines.UMLRTStateMachineExecution;

public class Calur_ExecutionFactory extends SM_ExecutionFactory {// SM_ExecutionFactory

	public ISemanticVisitor instantiateVisitor(Element element) {
		ISemanticVisitor visitor = null;
		
		if (element instanceof org.eclipse.uml2.uml.Class) {
			visitor = new CapsuleExecution();
		}
		else if (element instanceof StateMachine) {
			visitor = new UMLRTStateMachineExecution();
		}
		else {
			visitor = super.instantiateVisitor(element);
		}
		return visitor;
	}
	
	@Override
	public IOpaqueBehaviorExecution instantiateOpaqueBehaviorExecution(OpaqueBehavior behavior) {
		
		//return super.instantiateOpaqueBehaviorExecution(behavior);
		 int i = 0,
			languageIndex = -1;
		
		for (String language: behavior.getLanguages()) {
			if ("Calur".equals(language)) {
				languageIndex = i;
				break;
			}
			else {
				i++;
			}
		}
		if (languageIndex != -1) {
			String body = behavior.getBodies().get(languageIndex);
			System.out.println(body);
			CalurBehaviorExecution execution = (CalurBehaviorExecution) new CalurBehaviorExecution(behavior).copy();
			return execution;
			
		}
		return super.instantiateOpaqueBehaviorExecution(behavior);
	}
}
