package ca.queensu.cs.umlrt.calur.ui.providers;

import org.eclipse.core.internal.resources.WorkspaceRoot;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.xtext.resource.FileExtensionProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.embedded.IEditedResourceProvider;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;

public class CalurResourceProvider implements IEditedResourceProvider {

	@Inject private IResourceSetProvider resourceSetProvider;
	@Inject private FileExtensionProvider ext;
	
	@Override
	public XtextResource createResource() {
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		IProject project = ((FileEditorInput)editorPart.getEditorInput()).getFile().getProject();
		System.out.println(project);
//		ResourceSet resourceSet = ((PapyrusMultiDiagramEditor)editorPart).getEditingDomain().getResourceSet();
		ResourceSet resourceSet = resourceSetProvider.get(project);
		URI uri = URI.createURI("platfom:/resource/PingPong/temp." + ext.getPrimaryFileExtension());
		XtextResource result = (XtextResource) resourceSet.createResource(uri);
		resourceSet.getResources().add(result);
		return result;
	}

}
