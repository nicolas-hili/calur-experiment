/*
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.ide

import ca.queensu.cs.umlrt.calur.CalurRuntimeModule
import ca.queensu.cs.umlrt.calur.CalurStandaloneSetup
import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class CalurIdeSetup extends CalurStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new CalurRuntimeModule, new CalurIdeModule))
	}
	
}
