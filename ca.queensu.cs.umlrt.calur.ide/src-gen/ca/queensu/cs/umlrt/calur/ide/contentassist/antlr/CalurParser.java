/*
 * generated by Xtext 2.12.0
 */
package ca.queensu.cs.umlrt.calur.ide.contentassist.antlr;

import ca.queensu.cs.umlrt.calur.ide.contentassist.antlr.internal.InternalCalurParser;
import ca.queensu.cs.umlrt.calur.services.CalurGrammarAccess;
import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class CalurParser extends AbstractContentAssistParser {

	@Inject
	private CalurGrammarAccess grammarAccess;

	private Map<AbstractElement, String> nameMappings;

	@Override
	protected InternalCalurParser createParser() {
		InternalCalurParser result = new InternalCalurParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getStatementAccess().getAlternatives(), "rule__Statement__Alternatives");
					put(grammarAccess.getTimerStatementAccess().getAlternatives(), "rule__TimerStatement__Alternatives");
					put(grammarAccess.getExprAccess().getAlternatives(), "rule__Expr__Alternatives");
					put(grammarAccess.getVariableRefAccess().getAlternatives(), "rule__VariableRef__Alternatives");
					put(grammarAccess.getFieldAccess().getAlternatives(), "rule__Field__Alternatives");
					put(grammarAccess.getUnaryOperatorAccess().getAlternatives(), "rule__UnaryOperator__Alternatives");
					put(grammarAccess.getZeroaryOperatorAccess().getAlternatives(), "rule__ZeroaryOperator__Alternatives");
					put(grammarAccess.getBinaryOperatorAccess().getAlternatives(), "rule__BinaryOperator__Alternatives");
					put(grammarAccess.getBinaryBooleanOperatorAccess().getAlternatives(), "rule__BinaryBooleanOperator__Alternatives");
					put(grammarAccess.getUnaryArithmeticOperatorAccess().getAlternatives(), "rule__UnaryArithmeticOperator__Alternatives");
					put(grammarAccess.getRTSFunctionSymbolAccess().getAlternatives(), "rule__RTSFunctionSymbol__Alternatives");
					put(grammarAccess.getBinaryArithmeticOperatorAccess().getAlternatives(), "rule__BinaryArithmeticOperator__Alternatives");
					put(grammarAccess.getIndexAccess().getAlternatives(), "rule__Index__Alternatives");
					put(grammarAccess.getLiteralAccess().getAlternatives(), "rule__Literal__Alternatives");
					put(grammarAccess.getBoolAccess().getAlternatives(), "rule__Bool__Alternatives");
					put(grammarAccess.getUnitAccess().getAlternatives(), "rule__Unit__Alternatives");
					put(grammarAccess.getActionUncalledAccess().getGroup(), "rule__ActionUncalled__Group__0");
					put(grammarAccess.getVerbatimStatementAccess().getGroup(), "rule__VerbatimStatement__Group__0");
					put(grammarAccess.getSendStatementAccess().getGroup(), "rule__SendStatement__Group__0");
					put(grammarAccess.getSendStatementAccess().getGroup_6(), "rule__SendStatement__Group_6__0");
					put(grammarAccess.getLogStatementAccess().getGroup(), "rule__LogStatement__Group__0");
					put(grammarAccess.getLogStatementAccess().getGroup_2(), "rule__LogStatement__Group_2__0");
					put(grammarAccess.getInformInTimerStatementAccess().getGroup(), "rule__InformInTimerStatement__Group__0");
					put(grammarAccess.getInformInTimerStatementAccess().getGroup_0(), "rule__InformInTimerStatement__Group_0__0");
					put(grammarAccess.getInformInTimerStatementAccess().getGroup_4(), "rule__InformInTimerStatement__Group_4__0");
					put(grammarAccess.getInformInTimerStatementAccess().getGroup_5(), "rule__InformInTimerStatement__Group_5__0");
					put(grammarAccess.getInformEveryTimerStatementAccess().getGroup(), "rule__InformEveryTimerStatement__Group__0");
					put(grammarAccess.getInformEveryTimerStatementAccess().getGroup_0(), "rule__InformEveryTimerStatement__Group_0__0");
					put(grammarAccess.getInformEveryTimerStatementAccess().getGroup_4(), "rule__InformEveryTimerStatement__Group_4__0");
					put(grammarAccess.getInformEveryTimerStatementAccess().getGroup_5(), "rule__InformEveryTimerStatement__Group_5__0");
					put(grammarAccess.getCancelTimerStatementAccess().getGroup(), "rule__CancelTimerStatement__Group__0");
					put(grammarAccess.getTimeSpecAccess().getGroup(), "rule__TimeSpec__Group__0");
					put(grammarAccess.getIncarnateStatementAccess().getGroup(), "rule__IncarnateStatement__Group__0");
					put(grammarAccess.getIncarnateStatementAccess().getGroup_0(), "rule__IncarnateStatement__Group_0__0");
					put(grammarAccess.getDestroyStatementAccess().getGroup(), "rule__DestroyStatement__Group__0");
					put(grammarAccess.getImportStatementAccess().getGroup(), "rule__ImportStatement__Group__0");
					put(grammarAccess.getImportStatementAccess().getGroup_0(), "rule__ImportStatement__Group_0__0");
					put(grammarAccess.getDeportStatementAccess().getGroup(), "rule__DeportStatement__Group__0");
					put(grammarAccess.getRegisterStatementAccess().getGroup(), "rule__RegisterStatement__Group__0");
					put(grammarAccess.getDeregisterStatementAccess().getGroup(), "rule__DeregisterStatement__Group__0");
					put(grammarAccess.getExprAccess().getGroup_4(), "rule__Expr__Group_4__0");
					put(grammarAccess.getVariableInitializationStatementAccess().getGroup(), "rule__VariableInitializationStatement__Group__0");
					put(grammarAccess.getLocalVariableAccess().getGroup(), "rule__LocalVariable__Group__0");
					put(grammarAccess.getCallStatementAccess().getGroup(), "rule__CallStatement__Group__0");
					put(grammarAccess.getReturnStatementAccess().getGroup(), "rule__ReturnStatement__Group__0");
					put(grammarAccess.getSrandStatementAccess().getGroup(), "rule__SrandStatement__Group__0");
					put(grammarAccess.getVariableAffectationStatementAccess().getGroup(), "rule__VariableAffectationStatement__Group__0");
					put(grammarAccess.getGlobalVariableRefAccess().getGroup(), "rule__GlobalVariableRef__Group__0");
					put(grammarAccess.getGlobalVariableRefAccess().getGroup_2(), "rule__GlobalVariableRef__Group_2__0");
					put(grammarAccess.getProtocolVariableRefAccess().getGroup(), "rule__ProtocolVariableRef__Group__0");
					put(grammarAccess.getProtocolVariableRefAccess().getGroup_1(), "rule__ProtocolVariableRef__Group_1__0");
					put(grammarAccess.getOperationAccess().getGroup(), "rule__Operation__Group__0");
					put(grammarAccess.getArgumentsAccess().getGroup(), "rule__Arguments__Group__0");
					put(grammarAccess.getArgumentsAccess().getGroup_1(), "rule__Arguments__Group_1__0");
					put(grammarAccess.getObjectCallOperationAccess().getGroup(), "rule__ObjectCallOperation__Group__0");
					put(grammarAccess.getIfStatementAccess().getGroup(), "rule__IfStatement__Group__0");
					put(grammarAccess.getIfStatementAccess().getGroup_6(), "rule__IfStatement__Group_6__0");
					put(grammarAccess.getUnaryExprAccess().getGroup(), "rule__UnaryExpr__Group__0");
					put(grammarAccess.getZeroaryExprAccess().getGroup(), "rule__ZeroaryExpr__Group__0");
					put(grammarAccess.getBinaryExprAccess().getGroup(), "rule__BinaryExpr__Group__0");
					put(grammarAccess.getBinaryExprAccess().getGroup_1(), "rule__BinaryExpr__Group_1__0");
					put(grammarAccess.getPortRefAccess().getGroup(), "rule__PortRef__Group__0");
					put(grammarAccess.getPortRefAccess().getGroup_1(), "rule__PortRef__Group_1__0");
					put(grammarAccess.getPartRefAccess().getGroup(), "rule__PartRef__Group__0");
					put(grammarAccess.getPartRefAccess().getGroup_1(), "rule__PartRef__Group_1__0");
					put(grammarAccess.getLiteralNullAccess().getGroup(), "rule__LiteralNull__Group__0");
					put(grammarAccess.getLiteralBooleanAccess().getGroup(), "rule__LiteralBoolean__Group__0");
					put(grammarAccess.getLiteralIntegerAccess().getGroup(), "rule__LiteralInteger__Group__0");
					put(grammarAccess.getLiteralRealAccess().getGroup(), "rule__LiteralReal__Group__0");
					put(grammarAccess.getLiteralStringAccess().getGroup(), "rule__LiteralString__Group__0");
					put(grammarAccess.getActionAccess().getStatementsAssignment(), "rule__Action__StatementsAssignment");
					put(grammarAccess.getActionUncalledAccess().getContainerAssignment_0(), "rule__ActionUncalled__ContainerAssignment_0");
					put(grammarAccess.getActionUncalledAccess().getFeatureNameAssignment_1(), "rule__ActionUncalled__FeatureNameAssignment_1");
					put(grammarAccess.getVerbatimStatementAccess().getValueAssignment_1(), "rule__VerbatimStatement__ValueAssignment_1");
					put(grammarAccess.getSendStatementAccess().getOperationAssignment_2(), "rule__SendStatement__OperationAssignment_2");
					put(grammarAccess.getSendStatementAccess().getPortRefAssignment_5(), "rule__SendStatement__PortRefAssignment_5");
					put(grammarAccess.getSendStatementAccess().getArgumentsAssignment_6_2(), "rule__SendStatement__ArgumentsAssignment_6_2");
					put(grammarAccess.getLogStatementAccess().getValueAssignment_1(), "rule__LogStatement__ValueAssignment_1");
					put(grammarAccess.getLogStatementAccess().getArgumentsAssignment_2_2(), "rule__LogStatement__ArgumentsAssignment_2_2");
					put(grammarAccess.getInformInTimerStatementAccess().getTimerIdAssignment_0_0(), "rule__InformInTimerStatement__TimerIdAssignment_0_0");
					put(grammarAccess.getInformInTimerStatementAccess().getTimeSpecAssignment_3(), "rule__InformInTimerStatement__TimeSpecAssignment_3");
					put(grammarAccess.getInformInTimerStatementAccess().getTimeSpecAssignment_4_1(), "rule__InformInTimerStatement__TimeSpecAssignment_4_1");
					put(grammarAccess.getInformInTimerStatementAccess().getPortAssignment_5_1(), "rule__InformInTimerStatement__PortAssignment_5_1");
					put(grammarAccess.getInformEveryTimerStatementAccess().getTimerIdAssignment_0_0(), "rule__InformEveryTimerStatement__TimerIdAssignment_0_0");
					put(grammarAccess.getInformEveryTimerStatementAccess().getTimeSpecAssignment_3(), "rule__InformEveryTimerStatement__TimeSpecAssignment_3");
					put(grammarAccess.getInformEveryTimerStatementAccess().getTimeSpecAssignment_4_1(), "rule__InformEveryTimerStatement__TimeSpecAssignment_4_1");
					put(grammarAccess.getInformEveryTimerStatementAccess().getPortAssignment_5_1(), "rule__InformEveryTimerStatement__PortAssignment_5_1");
					put(grammarAccess.getCancelTimerStatementAccess().getTimerIdAssignment_1(), "rule__CancelTimerStatement__TimerIdAssignment_1");
					put(grammarAccess.getTimeSpecAccess().getValAssignment_0(), "rule__TimeSpec__ValAssignment_0");
					put(grammarAccess.getTimeSpecAccess().getUnitAssignment_1(), "rule__TimeSpec__UnitAssignment_1");
					put(grammarAccess.getIncarnateStatementAccess().getCapsuleIdAssignment_0_0(), "rule__IncarnateStatement__CapsuleIdAssignment_0_0");
					put(grammarAccess.getIncarnateStatementAccess().getCapsuleNameAssignment_2(), "rule__IncarnateStatement__CapsuleNameAssignment_2");
					put(grammarAccess.getIncarnateStatementAccess().getPartRefAssignment_4(), "rule__IncarnateStatement__PartRefAssignment_4");
					put(grammarAccess.getDestroyStatementAccess().getPartRefAssignment_1(), "rule__DestroyStatement__PartRefAssignment_1");
					put(grammarAccess.getImportStatementAccess().getCapsuleIdAssignment_0_0(), "rule__ImportStatement__CapsuleIdAssignment_0_0");
					put(grammarAccess.getImportStatementAccess().getCapsuleNameAssignment_2(), "rule__ImportStatement__CapsuleNameAssignment_2");
					put(grammarAccess.getImportStatementAccess().getPartRefAssignment_4(), "rule__ImportStatement__PartRefAssignment_4");
					put(grammarAccess.getDeportStatementAccess().getPartRefAssignment_1(), "rule__DeportStatement__PartRefAssignment_1");
					put(grammarAccess.getRegisterStatementAccess().getPortSourceAssignment_1(), "rule__RegisterStatement__PortSourceAssignment_1");
					put(grammarAccess.getRegisterStatementAccess().getPortDestAssignment_3(), "rule__RegisterStatement__PortDestAssignment_3");
					put(grammarAccess.getDeregisterStatementAccess().getPortAssignment_1(), "rule__DeregisterStatement__PortAssignment_1");
					put(grammarAccess.getVariableInitializationStatementAccess().getLocalVarAssignment_1(), "rule__VariableInitializationStatement__LocalVarAssignment_1");
					put(grammarAccess.getLocalVariableAccess().getNameAssignment_1(), "rule__LocalVariable__NameAssignment_1");
					put(grammarAccess.getLocalVariableAccess().getTypeAssignment_3(), "rule__LocalVariable__TypeAssignment_3");
					put(grammarAccess.getCallStatementAccess().getVarRefAssignment_1(), "rule__CallStatement__VarRefAssignment_1");
					put(grammarAccess.getReturnStatementAccess().getExpreAssignment_1(), "rule__ReturnStatement__ExpreAssignment_1");
					put(grammarAccess.getSrandStatementAccess().getSeedAssignment_2(), "rule__SrandStatement__SeedAssignment_2");
					put(grammarAccess.getVariableAffectationStatementAccess().getVarRefAssignment_0(), "rule__VariableAffectationStatement__VarRefAssignment_0");
					put(grammarAccess.getVariableAffectationStatementAccess().getExprAssignment_2(), "rule__VariableAffectationStatement__ExprAssignment_2");
					put(grammarAccess.getGlobalVariableRefAccess().getSegmentsAssignment_1(), "rule__GlobalVariableRef__SegmentsAssignment_1");
					put(grammarAccess.getGlobalVariableRefAccess().getSegmentsAssignment_2_1(), "rule__GlobalVariableRef__SegmentsAssignment_2_1");
					put(grammarAccess.getProtocolVariableRefAccess().getSegmentsAssignment_0(), "rule__ProtocolVariableRef__SegmentsAssignment_0");
					put(grammarAccess.getProtocolVariableRefAccess().getSegmentsAssignment_1_1(), "rule__ProtocolVariableRef__SegmentsAssignment_1_1");
					put(grammarAccess.getPropertyAccess().getPropertyAssignment(), "rule__Property__PropertyAssignment");
					put(grammarAccess.getOperationAccess().getOperationAssignment_0(), "rule__Operation__OperationAssignment_0");
					put(grammarAccess.getOperationAccess().getArgumentsAssignment_2(), "rule__Operation__ArgumentsAssignment_2");
					put(grammarAccess.getArgumentsAccess().getArgumentsAssignment_0(), "rule__Arguments__ArgumentsAssignment_0");
					put(grammarAccess.getArgumentsAccess().getArgumentsAssignment_1_1(), "rule__Arguments__ArgumentsAssignment_1_1");
					put(grammarAccess.getObjectCallOperationAccess().getVarAssignment_0(), "rule__ObjectCallOperation__VarAssignment_0");
					put(grammarAccess.getObjectCallOperationAccess().getOperationAssignment_2(), "rule__ObjectCallOperation__OperationAssignment_2");
					put(grammarAccess.getIfStatementAccess().getExprAssignment_1(), "rule__IfStatement__ExprAssignment_1");
					put(grammarAccess.getIfStatementAccess().getThenStatementsAssignment_4(), "rule__IfStatement__ThenStatementsAssignment_4");
					put(grammarAccess.getIfStatementAccess().getElseStatementsAssignment_6_2(), "rule__IfStatement__ElseStatementsAssignment_6_2");
					put(grammarAccess.getUnaryExprAccess().getOperatorAssignment_0(), "rule__UnaryExpr__OperatorAssignment_0");
					put(grammarAccess.getUnaryExprAccess().getExprAssignment_2(), "rule__UnaryExpr__ExprAssignment_2");
					put(grammarAccess.getZeroaryExprAccess().getOperatorAssignment_0(), "rule__ZeroaryExpr__OperatorAssignment_0");
					put(grammarAccess.getBinaryExprAccess().getFirstAssignment_0(), "rule__BinaryExpr__FirstAssignment_0");
					put(grammarAccess.getBinaryExprAccess().getOperatorAssignment_1_0(), "rule__BinaryExpr__OperatorAssignment_1_0");
					put(grammarAccess.getBinaryExprAccess().getLastAssignment_1_1(), "rule__BinaryExpr__LastAssignment_1_1");
					put(grammarAccess.getPortRefAccess().getPortAssignment_0(), "rule__PortRef__PortAssignment_0");
					put(grammarAccess.getPortRefAccess().getIndexAssignment_1_1(), "rule__PortRef__IndexAssignment_1_1");
					put(grammarAccess.getPartRefAccess().getPartAssignment_0(), "rule__PartRef__PartAssignment_0");
					put(grammarAccess.getPartRefAccess().getIndexAssignment_1_1(), "rule__PartRef__IndexAssignment_1_1");
					put(grammarAccess.getLiteralBooleanAccess().getValueAssignment_1(), "rule__LiteralBoolean__ValueAssignment_1");
					put(grammarAccess.getLiteralIntegerAccess().getValueAssignment_1(), "rule__LiteralInteger__ValueAssignment_1");
					put(grammarAccess.getLiteralRealAccess().getValueAssignment_1(), "rule__LiteralReal__ValueAssignment_1");
					put(grammarAccess.getLiteralStringAccess().getValueAssignment_1(), "rule__LiteralString__ValueAssignment_1");
				}
			};
		}
		return nameMappings.get(element);
	}
			
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public CalurGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(CalurGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
